# -*- coding: utf-8 -*-
from odoo import models, api, fields, _


class TicketFaqCategory(models.Model):
    _name = "ticket.faq.category"

    name = fields.Char(
        string="Name",
        required=True,
    )
