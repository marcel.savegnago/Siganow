# -*- coding: utf-8 -*-
from odoo import models, api, fields, _


class TicketFaqTags(models.Model):
    _name = "ticket.faq.tags"

    name = fields.Char(
        string="Name",
        required=True,
    )
