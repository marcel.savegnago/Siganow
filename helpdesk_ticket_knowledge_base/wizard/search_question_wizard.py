# -*- coding: utf-8 -*-
from openerp import models, api, fields, _


class HelpdeskSearchQuestionWizard(models.TransientModel):
    _name = "helpdesk.search.question.wizard"

    ticket_faq_ids = fields.Many2many(
        "ticket.faq",
        string="Question & Answers"
    )
