# -*- coding: utf-8 -*-
# Copyright 2016 LasLabs Inc.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import api, models
from lxml import etree


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False,
                        submenu=False):

        ret_val = super(ResConfigSettings, self).fields_view_get(
            view_id=view_id,
            view_type=view_type,
            toolbar=toolbar,
            submenu=submenu,
        )

        page_name = ret_val['name']
        doc = etree.XML(ret_val['arch'])

        queries = []
        replace_query=[]
        if page_name == 'res.config.settings.view.form':
            print ("---page_name--------",page_name)
            queries += [
                # acounting

                "//div[field[@name='account_hide_setup_bar' and "
                "@widget='upgrade_boolean']]",
                "//div[field[@name='module_currency_rate_live' and "
                "@widget='upgrade_boolean']]",
                "//div[field[@name='module_account_reports_followup' and \
                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_batch_deposit' and "
                "@widget='upgrade_boolean']]",
                "//div[field[@name='module_account_sepa_direct_debit' and "
                "@widget='upgrade_boolean']]",
                "//div[field[@name='module_l10n_us_check_printing' and \
                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_sepa' and \
                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_reports' and \
                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_yodlee' and \
                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_bank_statement_import_csv' "
                "and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_bank_statement_import_qif' "
                "and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_bank_statement_import_ofx' "
                "and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_bank_statement_import_camt' "
                "and @widget='upgrade_boolean']]",
                "//div[field[@name='module_print_docsaway' and "
                "@widget='upgrade_boolean']]",
                "//div[field[@name='module_account_deferred_revenue' and \
                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_taxcloud' and \
                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_batch_payment' and \
                                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='qr_code' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_check_printing' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_budget' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_intrastat' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_asset' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_account_invoice_extract' and @widget='upgrade_boolean']]",

                # sale
                "//div[field[@name='module_sale_coupon' and \
                                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_delivery_ups' and \
                                                    @widget='upgrade_boolean']]",
                "//div[field[@name='module_delivery_dhl' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_delivery_fedex' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_delivery_usps' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_delivery_bpost' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_delivery_easypost' and @widget='upgrade_boolean']]",

                # event
                "//div[field[@name='module_event_barcode' and @widget='upgrade_boolean']]",

                # crm
                "//div[field[@name='module_web_clearbit' and @widget='upgrade_boolean']]",

                # mrp
                "//div[field[@name='group_mrp_routings' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_mrp_maintenance' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_mrp_mps' and @widget='upgrade_boolean']]",
                "//div[field[@name='module_quality_control' and @widget='upgrade_boolean']]",

                # stock
                "//div[field[@name='module_stock_barcode' and @widget='upgrade_boolean']]",

                # l10n_mx
                "//div[field[@name='module_l10n_mx_edi' and @widget='upgrade_boolean']]",

                # project
                "//div[field[@name='module_project_forecast' and @widget='upgrade_boolean']]",

                # hr_payroll
                "//div[field[@name='module_account_accountant' and @widget='upgrade_boolean']]",

                # website
                "//div[field[@name='module_website_version' and @widget='upgrade_boolean']]",

                # hr_timesheet
                "//div[field[@name='module_project_timesheet_synchro' and @widget='upgrade_boolean']]",

                # purchase
                "//div[field[@name='module_account_3way_match' and @widget='upgrade_boolean']]",

                # base_setup
                "//div[field[@name='module_inter_company_rules' and @widget='upgrade_boolean']]",

                # pos
                "//div[field[@name='module_pos_loyalty' and @widget='upgrade_boolean']]",
            ]

        for query in queries:
            for item in doc.xpath(query):
                print ("-------------",self._cr.dbname)
                item.getparent().set('style','display:none')
        ret_val['arch'] = etree.tostring(doc)
        return ret_val
