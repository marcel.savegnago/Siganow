# -*- coding: utf-8 -*-
# Copyright 2016 LasLabs Inc.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    'name': 'Settings - Remove Enterprise Fields',
    'summary': 'Remove fields in all settings views marked as enterprise',
    'version': '10.0.1.0.0',
    'category': 'Maintenance',
    'website': "https://laslabs.com",
    'author': 'Planet-Odoo',
    'license': 'AGPL-3',
    'depends': [
        'base',
    ],
    'data': [
            'delete_enterprise_modules.xml',
    ],
    'application': False,
    'installable': True,
}
