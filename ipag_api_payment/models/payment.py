from odoo import api,fields,_,models,http
from odoo.http import request
from odoo.http import HttpRequest
import requests
import odoo
from odoo.addons.website_sale.controllers.main import WebsiteSale

class IpagPayment(WebsiteSale):

    @http.route('/ipag/payment', type='http', auth='public', website=True ,csrf=False)
    def show_ipag_form(self, **post):
        vals = {'identification':'paulo.batista@growxi.com.br',
                'operation':'Payment',
                'value':'500.00',}
        return http.request.render("ipag_api_payment.ipag_form",{'data':vals})
    # @http.route(['/shop/payment/transaction/',
    #              '/shop/payment/transaction/<int:so_id>',
    #              '/shop/payment/transaction/<int:so_id>/<string:access_token>'], type='json', auth="public",website=True)
    # def payment_transaction(self, acquirer_id, save_token=False, so_id=None, access_token=None, token=None, **kwargs):
    #     head = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36','username':'paulo.batista@growxi.com.br','password':'C7A6-1D6831E0-466874B5-6B0C9BF2-004C'}
    #     # resp = requests.get('https://sandbox.ipag.com.br', headers=head)
    #     vals = {'identification':'paulo.batista@growxi.com.br',
    #             'method':'Visa',
    #             'operation':'Payment',
    #             'request':'001002',
    #             'return_url':'http://localhost:8012/shop/confirmation',
    #             'return_type':'xml',
    #             'boleto_type':'xml',
    #             'value':100.00,
    #             'name':'Amardeep'
    #     }
    #     res = super(Website_Sale,self).payment_transaction(acquirer_id, save_token, so_id, access_token, token, **kwargs)
    #     order = request.website.sale_get_order()
    #     acquirer_obj = request.env['payment.acquirer']
    #     acquirer = acquirer_obj.browse(acquirer_id)
    #     qcontext = request.params.copy()
    #     # if acquirer.name == 'IPAG':
    #     #   return acquirer.view_template_id.render(vals, engine='ir.qweb')
    #     return res

class PaymentAcquirer(models.Model):
    _inherit = 'payment.acquirer'

    provider = fields.Selection(selection_add=[('ipag','iPag')])
    ipag_id = fields.Char('IPAG-ID')
    ipag_key = fields.Char('IPAG-KEY')

    @http.route('/ipag/payment', type='http', auth="public")
    def show_ipag_details(self, qcontext, redirect=None, **kw):
        return http.request.render('ipag_api_payment.ipag_form',qcontext)

    # @api.multi
    # def render_ipag_form(self):
    #     # head = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
    #     #               'username':'paulo.batista@growxi.com.br',
    #     #               'password':'C7A6-1D6831E0-466874B5-6B0C9BF2-004C',
    #     #               'Content-Type':'application/xml',
    #     #               'Accept':'application/xml',
    #     #               'identification':'paulo.batista@growxi.com.br',
    #     #               'operation':'payment','value':'100.00',
    #     #         'plugin':'html',}
    #     # chck = requests.post(url='https://sandbox.ipag.com.br/vpos', headers=head,)
    #     # resp = requests.post('https://sandbox.ipag.com.br', headers=head)
    #     # response = requests.get('https://sandbox.ipag.com.br/service/sessionToken',headers={"Content-Type": "application/json",
    #     #                                                                                      "Authorization": "Basic am9uYXRoYW46REM4QS00QzE2ONHSNS1EQTZBRUY2O92SDRkQ2RDMyOC0wRjAz"})
    #     template = HttpRequest.render(self,template='ipag_form')
    #     return template
