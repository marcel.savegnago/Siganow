{
    'name': 'iPag Api Integration',
    'version': '12.0',
    'summary': 'Integrate iPag for Payment Gateway',
    'category': 'API Integration Payment',
    'description': """Integration with iPag for payment done after purchasing the services and packages""",
    'author': 'Planet Odoo',
    'website': 'http://www.planet-odoo.com/',
    'depends': ['base','payment'],
    'data': [
        "static/src/xml/ipag.xml",
        "views/payment.xml",
    ],
    'images': [],
    'qweb': [],
    'installable': True,
    'application': False,
}