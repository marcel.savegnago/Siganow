from odoo import api, fields, models, _
from odoo.exceptions import UserError, Warning


class CrmTimesheet(models.Model):

    _inherit = 'account.analytic.line'

    product_category = fields.Many2one('product.category', string='Product Category')
    cost_rate = fields.Integer('Cost Rate', compute='get_costdata', store=True)
    tot_cost = fields.Integer('Total Cost', compute='get_tot', store=True)
    date_start = fields.Datetime("Start Date")
    date_stop = fields.Datetime("End Date")

    @api.one
    @api.depends('product_category','date_stop')
    def get_costdata(self):
          for rec in self:
            if rec.support_request_id:
                for each in rec.support_request_id.timesheet_line_ids:

                    # emp_obj = self.env['account.analytic.line'].search(
                    #     [('employee_id', '=', each.id), ('project_id', '!=', False)])

                    # for emp in emp_obj:
                    cust_cost_rate = 0.0
                    for j in each.employee_id.cost_timesheet_line:
                        if each.product_category.id == j.product_category.id:
                            if j.start_date:
                                if each.date:
                                    if j.end_date:
                                        if j.start_date < each.date < j.end_date:
                                            cust_cost_rate = j.cost
                                            break
                        # elif j.default_check == True:
                        #       each.cost_rate= j.cost

                        if not cust_cost_rate:
                            if each.employee_id.cost_timesheet_line:
                                cost_id = each.employee_id.cost_timesheet_line.search([('default_check', '=', True),('cost_ids','=',each.employee_id.id)])
                            cust_cost_rate = cost_id.cost
                        if cust_cost_rate:
                            each.cost_rate = cust_cost_rate
            else:
                for each in rec:

                    # emp_obj = self.env['account.analytic.line'].search(
                    #     [('employee_id', '=', each.id), ('project_id', '!=', False)])

                    # for emp in emp_obj:
                    cust_cost_rate = 0.0
                    for j in each.employee_id.cost_timesheet_line:
                        if each.product_category.id == j.product_category.id:
                            if j.start_date:
                                if each.date:
                                    if j.end_date:
                                        if j.start_date < each.date < j.end_date:
                                            cust_cost_rate = j.cost
                                            break
                        # elif j.default_check == True:
                        #       each.cost_rate= j.cost
                    if not cust_cost_rate:
                        if each.employee_id.cost_timesheet_line:
                            cost_id = each.employee_id.cost_timesheet_line.search([('default_check', '=', True),('cost_ids','=',each.employee_id.id)])
                            cust_cost_rate = cost_id.cost
                    if cust_cost_rate:
                        each.cost_rate = cust_cost_rate

                    # each.emp_timesheet_line = emp_obj

    @api.one
    @api.depends('cost_rate')
    def get_tot(self):
        for rec in self:
            if rec.employee_id:
        # user = self.env['hr.employee'].search([('id', '=', self.employee_id.id)])
        # for i in user:
        #     for j in i.emp_timesheet_line:
                rec.tot_cost = rec.cost_rate * rec.unit_amount

class CrmLeadOpportunity(models.Model):

    _inherit = 'hr.expense'

    lead_opportunity_id = fields.Many2one('crm.lead', string='Lead/Opportunity')
    # project_name = fields.Many2one('project.project', string='Project', compute="project_name_change")
    project_name = fields.Many2one('project.project', string='Project')

    expense_planned_expense_amount = fields.Float(string="Planned Expense Amount", store=True)
    expense_real_expense_amount = fields.Float(string="Real Expense Amount", store=True)
    project_readonly = fields.Boolean("Project Readonly")

    @api.onchange('project_name')
    def get_planned_real_expense(self):
        if self.project_name:
            self.expense_planned_expense_amount = self.project_name.planned_expense_amount
            self.expense_real_expense_amount = self.project_name.real_expense_amount

    @api.model
    def create(self, values):
        if self.user_has_groups('siga_erp_custom.expense_budget_group'):
            if values.get('unit_amount') and values.get('project_name'):
                project_search = self.env['hr.expense'].search([('project_name', '=', values.get('project_name'))])
                project_brw = self.env['project.project'].search([('id', '=', values.get('project_name'))])
                total_val = 0
                for rec in project_search:
                    total_val = total_val + rec.total_amount
                total_val = total_val + (values.get('unit_amount') * values.get('quantity'))
                if total_val <= project_brw.planned_expense_amount:
                    project_brw.write({'real_expense_amount': total_val})
                else:
                    raise Warning(_('Your Expenses amount is exceeding the Planned Expenses amount'))
                values['project_readonly'] = True
        return super(CrmLeadOpportunity, self).create(values)

    @api.multi
    def write(self, values):
        if self.user_has_groups('siga_erp_custom.expense_budget_group'):
            if values.get('unit_amount') or values.get('quantity') or values.get('project_name'):

                if values.get('unit_amount') and values.get('quantity'):
                    project_search = self.env['hr.expense'].search([('project_name', '=', self.project_name.id), ('id', '!=', self.id)])
                    project_brw = self.env['project.project'].search([('id', '=', self.project_name.id)])
                    total_val = 0
                    for rec in project_search:
                        total_val = total_val + rec.total_amount
                    total_val = total_val + (values.get('unit_amount') * values.get('quantity'))
                    if total_val <= project_brw.planned_expense_amount:
                        project_brw.write({'real_expense_amount': total_val})
                    else:
                        raise Warning(_('Your Expenses amount is exceeding the Planned Expenses amount'))

                elif values.get('unit_amount') or values.get('quantity'):
                    project_search = self.env['hr.expense'].search([('project_name', '=', self.project_name.id), ('id', '!=', self.id)])
                    project_brw = self.env['project.project'].search([('id', '=', self.project_name.id)])
                    total_val = 0
                    for rec in project_search:
                        total_val = total_val + rec.total_amount
                    if values.get('unit_amount'):
                        exp_amt = values.get('unit_amount') * self.quantity
                    elif values.get('quantity'):
                        exp_amt = self.unit_amount * values.get('quantity')
                    total_val = total_val + exp_amt
                    if total_val <= project_brw.planned_expense_amount:
                        project_brw.write({'real_expense_amount': total_val})
                    else:
                        raise Warning(_('Your Expenses amount is exceeding the Planned Expenses amount'))

                elif values.get('project_name'):
                    project_search = self.env['hr.expense'].search([('project_name', '=', values.get('project_name')), ('id', '!=', self.id)])
                    project_brw = self.env['project.project'].search([('id', '=', values.get('project_name'))])
                    total_val = 0
                    for rec in project_search:
                        total_val = total_val + rec.total_amount
                    total_val = total_val + self.total_amount
                    if total_val <= project_brw.planned_expense_amount:
                        project_brw.write({'real_expense_amount': total_val})
                    else:
                        raise Warning(_('Your Expenses amount is exceeding the Planned Expenses amount'))

        return super(CrmLeadOpportunity, self).write(values)





