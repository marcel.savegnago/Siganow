from odoo import fields,api,models,SUPERUSER_ID,_


class TicketCategory(models.Model):

    _name = 'ticket.category'

    name=fields.Char("Category")

    kanban_template_title = fields.Many2one('kanban.helpdesk.templates',
                                            string="Kanban Template Title")
    is_created = fields.Boolean("Created")

    ticket_count = fields.Integer('Count', compute='ticket_counts')

    @api.depends('name')
    def ticket_counts(self):
        tech_count = self.env['helpdesk.support'].read_group([('ticket_category','in',self.ids)], ['ticket_category'], ['ticket_category'])
        result = dict((data['ticket_category'][0], data['ticket_category_count']) for data in tech_count)
        for project in self:
            project.ticket_count = result.get(project.id, 0)

    @api.multi
    def write(self, vals):
        res = super(TicketCategory, self).write(vals)
        stage_ids = self.kanban_template_title.stages
        for stage in stage_ids:
            stage.update({'ticket_cat_ids': [(4,self.id)]})
        return res

    @api.model
    def create(self, vals):
        res = super(TicketCategory, self).create(vals)
        ctx = dict(self._context)
        ctx.update({'search_default_ticket_category': self.id, 'default_ticket_category': self.id})
        stage_ids = res.kanban_template_title.stages
        for stage in stage_ids:
            stage.update({'ticket_cat_ids': [(4,res.id)]})

        return res.with_context(ctx)

    @api.onchange('kanban_template_title')
    def field_read_only(self):
        if self.kanban_template_title:
            self.is_created = True

    @api.multi
    def open_tasks(self):
        ctx = dict(self._context)
        ctx.update({'search_default_ticket_category': self.id,'default_ticket_category':self.id})
        action = self.env['ir.actions.act_window'].for_xml_id('crm_custom', 'helpdesk_support_action')
        stage_ids = self.kanban_template_title.stages
        for stage in stage_ids:
            stage.update({'ticket_cat_ids': [(6, 0, [self.id])]})
        return dict(action, context=ctx)


class KanbanHelpesk(models.Model):

    _inherit = 'helpdesk.support'

    @api.depends('timesheet_line_ids', 'timesheet_line_ids.tot_cost')
    def _compute_timesheet_tot_cost(self):
        for line in self:
            line.timesheet_cost_amount = abs(sum(p.tot_cost for p in line.timesheet_line_ids))

    ticket_category=fields.Many2one('ticket.category',string="Category")

    timesheet_cost_amount = fields.Float(
        string="Total Timesheet Cost",
        compute="_compute_timesheet_tot_cost",
        copy=True,
        store=True,)


    # @api.model
    # def _read_group_stage_ids(self, stages, domain, order):
    #     # stages = self.env['helpdesk.stage.config'].search([('stage_type','=',False)])
    #     search_domain = [('id', 'in', stages.ids)]
    #     if 'default_ticket_category' in self.env.context:
    #         search_domain = ['|', ('ticket_cat_ids', '=', self.env.context['default_ticket_category'])] + search_domain
    #
    #     stage_ids = stages._search(search_domain, order=order, access_rights_uid=SUPERUSER_ID)
    #     return stages.browse(stage_ids)

class TicketStage(models.Model):

    _inherit = 'helpdesk.stage.config'

    def _get_default_category_ids(self):
        default_ticket_category = self.env.context.get('default_ticket_category')
        return [default_ticket_category] if default_ticket_category else None

    ticket_cat_ids=fields.Many2many('ticket.category','temps_help_id','help_temps_id','kanban_temps_id',default=_get_default_category_ids,string="Category")

    @api.multi
    def unlink(self):
        stages = self
        default_ticket_category = self.env.context.get('default_ticket_category')
        if default_ticket_category:
            shared_stages = self.filtered(lambda x: len(x.ticket_cat_ids) > 1 and default_ticket_category in x.ticket_cat_ids.ids)
            tasks = self.env['helpdesk.support'].with_context(active_test=False).search(
                [('ticket_category', '=', default_ticket_category), ('stage_id', 'in', self.ids)])
            if shared_stages and not tasks:
                shared_stages.write({'ticket_cat_ids': [(3, default_ticket_category)]})
                stages = self.filtered(lambda x: x not in shared_stages)
        return super(TicketStage, stages).unlink()


class KanbanTemplateHelpdesk(models.Model):

    _name = 'kanban.helpdesk.templates'
    _rec_name = 'template_title'

    template_title = fields.Char("Title", required='True')
    stages = fields.Many2many('helpdesk.stage.config', 'temp_kanbans_rel', 'stages_temp_id', 'kanbans_temp_id',
                              string="Stages")


class InheritCategory(models.TransientModel):

    _inherit = 'create.helpdesk.support.wizard'

    ticket_category=fields.Many2one('ticket.category',string="Category")


