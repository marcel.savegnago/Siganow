from odoo import api, fields, models


class CustomConfigSettingsMenu(models.TransientModel):

    _inherit = 'res.config.settings'

    group_support_task_configuration = fields.Boolean("Support / Task Configuration",
                                                      implied_group='crm_custom.support_task_configuration')
    group_timesheet_for_support_task = fields.Boolean("Timesheet for Support / Task",
                                                      implied_group='crm_custom.timesheet_for_support_task')
    group_expenses_for_crm_leads_opportunities = fields.Boolean("Expenses for CRM Leads/Opportunities",
                                                       implied_group='crm_custom.expenses_for_crm_leads_opportunities')

    @api.model
    def get_values(self):
        res = super(CustomConfigSettingsMenu, self).get_values()
        res.update({
            'group_support_task_configuration': self.env['ir.config_parameter'].sudo().get_param('group_support_task_configuration'),
            'group_timesheet_for_support_task': self.env['ir.config_parameter'].sudo().get_param('group_timesheet_for_support_task'),
            'group_expenses_for_crm_leads_opportunities': self.env['ir.config_parameter'].sudo().get_param('group_expenses_for_crm_leads_opportunities'),
        }
        )
        return res

    @api.multi
    def set_values(self):
        res = super(CustomConfigSettingsMenu, self).set_values()
        support_setting = self.env['ir.config_parameter'].sudo()
        support_setting.set_param('group_support_task_configuration', self.group_support_task_configuration)
        timesheet_setting = self.env['ir.config_parameter'].sudo().set_param('group_timesheet_for_support_task', self.group_timesheet_for_support_task)
        expenses_setting = self.env['ir.config_parameter'].sudo().set_param('group_expenses_for_crm_leads_opportunities', self.group_expenses_for_crm_leads_opportunities)

    @api.onchange('group_timesheet_for_support_task')
    def time_sheet_status(self):
        if self.group_timesheet_for_support_task == True:
            for item in self.env['helpdesk.support'].search([]):
                item.write({'read_timesheet': True})
        else:
            for item in self.env['helpdesk.support'].search([]):
                item.write({'read_timesheet': False})


class CrmLead(models.Model):

    _inherit = 'crm.lead'

    total_consumed_hours=fields.Float('Total Consumed Hours')
    total_cost_for_consumed_hours=fields.Float('Total Cost For Consumed Hours')
    total_expense=fields.Float('Total Expense')
    total_cost_for_opportunity=fields.Float('Total Cost For Opportunity')