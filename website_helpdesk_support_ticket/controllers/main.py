# -*- coding: utf-8 -*-

from collections import OrderedDict
import werkzeug
import json

import base64
from odoo import http, _
from odoo.http import request
import datetime
from odoo import models,registry, SUPERUSER_ID
# from odoo.addons.website_portal.controllers.main import website_account
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager, get_records_pager

from odoo.osv.expression import OR
import logging
_logger = logging.getLogger(__name__)

class HelpdeskSupport(http.Controller):

    def _check_domain(self, **post):
        return True

    @http.route(['/website_helpdesk_support_ticket/ticket_submitted'], type='http', auth="public", methods=['POST'], website=True)
    def ticket_submitted(self, **post):

        check_result = self._check_domain(**post)
        if not check_result:
            try:
                return request.render('helpdesk_domain_restriction.domain_ticket_email',{})
            except:
                pass

        cr, uid, context, pool = http.request.cr, http.request.uid, http.request.context, request.env
        contract_id = request.env['product.details.line'].sudo().browse(int(post['client']))
        Partner = request.env['res.partner'].sudo().search([('id', '=', contract_id.partner_id.id)], limit=1)
        
        if Partner:
            team_obj = http.request.env['support.team']
            team_match = request.env['support.team'].sudo().search([('is_team','=', True)], limit=1)

            if post.get('team', False):
                team_match = team_obj.sudo().browse(int(post['team']))

            analytic_id=request.env['account.analytic.account'].sudo().search([('partner_id', '=', Partner.id)],limit=1)
                
            support = pool['helpdesk.support'].create({
                                                            'subject': post['subject'],
                                                            'team_id':team_match.id,
                                                            'product_id':int(post['product']),
                                                            #'partner_id' :team_match.leader_id.id,
                                                            'user_id':team_match.leader_id.id,
                                                            'team_leader_id': team_match.leader_id.id,
                                                            'email': post['email'],
                                                            # 'phone': post['phone'],
                                                            'ticket_category': int(post['category']),
                                                            'description': post['description'],
                                                            'priority': post['priority'],
                                                            # 'client': post['client'],
                                                            'partner_id': Partner.id,
                                                            'version':post['version'],
                                                            'environment':post['env'],
                                                            'analytic_account_id':analytic_id.id,
                                                            'level_config_id':Partner.level_config_id.id
                                                             })
            
            values = {
                'support':support
            }
            
            attachment_list = request.httprequest.files.getlist('attachment')
            for image in attachment_list:
                if post.get('attachment'):
                    attachments = {
                               'res_name': image.filename,
                               'res_model': 'helpdesk.support',
                               'res_id': support,
                               'datas': base64.encodestring(image.read()),
                               'type': 'binary',
                               'datas_fname': image.filename,
                               'name': image.filename,
                           }
                    attachment_obj = http.request.env['ir.attachment']
                    attach = attachment_obj.sudo().create(attachments)
            if len(attachment_list) > 0:
                group_msg = 'Customer has sent %s attachments to this helpdesk ticket. Name of attachments are: ' % (len(attachment_list))
                for attach in attachment_list:
                    group_msg = group_msg + '\n' + attach.filename
                group_msg = group_msg + '\n'  +  '. You can see top attachment menu to download attachments.'
                support.sudo().message_post(body=_(group_msg),message_type='comment')
                    
            return request.render('website_helpdesk_support_ticket.thanks_mail_send', values)
        else:
            return request.render('website_helpdesk_support_ticket.support_invalid',{})

    
   
    @http.route(['/website_helpdesk_support_ticket/invite'], auth='public', website=True, methods=['POST'])
    def index_user_invite(self, **kw):
        email = kw.get('email')
        name = kw.get('name')
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        user = request.env['res.users'].browse(request.uid)
        user_exist = request.env['res.users'].sudo().search([('login','=',str(email))])
        vals = {
                  'user_id':user_exist,
                }
        if user_exist:
            return http.request.render('website_helpdesk_support_ticket.user_alredy_exist', vals)
        value={
              'name': name,
              'email': email,
              'invitation_date':datetime.date.today(),
              'referrer_user_id':user.id,
              }
        user_info_id = self.create_history(value)
        base_url = http.request.env['ir.config_parameter'].get_param('web.base.url', default='http://localhost:8069') + '/page/website_helpdesk_support_ticket.user_thanks'
        url = "%s?user_info=%s" %(base_url, user_info_id.id)
        reject_url = http.request.env['ir.config_parameter'].get_param('web.base.url', default='http://localhost:8069') + '/page/website_helpdesk_support_ticket.user_thanks_reject'
        rejected_url = "%s?user_info=%s" %(reject_url, user_info_id.id)
        local_context = http.request.env.context.copy()
        issue_template = http.request.env.ref('website_helpdesk_support_ticket.email_template_helpdesk_ticket')
        local_context.update({'user_email': email, 'url': url, 'name':name,'rejected_url':rejected_url})
        issue_template.sudo().with_context(local_context).send_mail(request.uid, force_send=True)
       
    @http.route(['/helpdesk_email/feedback/<int:order_id>'], type='http', auth='public', website=True)
    def feedback_email(self, order_id, **kw):
        values = {}
        ticket_obj_id = request.env['helpdesk.support'].browse(order_id)
        values.update({'ticket_id': ticket_obj_id,'ticket_rec_id':order_id})
        return request.render("website_helpdesk_support_ticket.website_helpdesk_feedback", values) 
       
    @http.route(['/helpdesk/feedback/'],
                type='http', auth='public', website=True)
    def start_rating(self, **kw):
        partner_id = kw['partner_id']
        user_id = kw['ticket_rec_id']
        ticket_obj = request.env['helpdesk.support'].browse(int(user_id))
        #if partner_id == UserInput.partner_id.id:
        vals = {
              'rating':kw['star'],
              'comment':kw['comment'],
            }
        ticket_obj.sudo().write(vals)
        customer_msg = _(ticket_obj.partner_id.name + 'has send this feedback rating is %s and comment is %s') % (kw['star'],kw['comment'],)
        ticket_obj.sudo().message_post(body=customer_msg)
        return http.request.render("website_helpdesk_support_ticket.successful_feedback")

    @http.route(['/website_support_ticket/search_user_ticket'], type='http', auth="user", methods=['POST'], website=True)
    def search_user_ticket(self, **post):
        Ticket = request.env['helpdesk.support'].sudo().search([('name', '=', post['ticket_no'])])
        if Ticket:
            portal_link = "/my/ticket/%s" %(Ticket.id)
            return werkzeug.utils.redirect(portal_link)
        else:
            return http.request.render('website_helpdesk_support_ticket.ticket_invalid',{})

# class website_account(website_account):

class CustomerPortal(CustomerPortal):
    _items_per_page = 20
#     @http.route()
#     def account(self, **kw):
#         """ Add ticket documents to main account page """
#         response = super(CustomerPortal, self).account(**kw)
#         partner = request.env.user.partner_id
#         ticket = request.env['helpdesk.support']
#         ticket_count = ticket.sudo().search_count([
#         ('partner_id', 'child_of', [partner.commercial_partner_id.id])
#           ])
#         response.qcontext.update({
#         'ticket_count': ticket_count,
#         })
#         return response
    
    def _prepare_portal_layout_values(self):
        values = super(CustomerPortal, self)._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        ticket = request.env['helpdesk.support']
        ticket_count = ticket.sudo().search_count([
        ('partner_id', 'child_of', [partner.commercial_partner_id.id])
          ])
        if not partner.child_ids:
            domain = [
                ('create_uid', '=', [request.env.user.id])
            ]
            ticket_count = ticket.sudo().search_count(domain)
        values.update({
            'ticket_count': ticket_count,
            'page_name': 'ticket_page',
        })
        return values
    
    
    @http.route(['/my/tickets', '/my/tickets/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_ticket(self, page=1, date_begin=None, date_end=None, sortby=None, filterby=None, search=None, search_in='content', **kw):
        response = super(CustomerPortal, self)
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        support_obj = http.request.env['helpdesk.support']
        domain = [
            ('partner_id', 'child_of', [partner.commercial_partner_id.id])
        ]
        # count for pager
        ticket_count = support_obj.sudo().search_count(domain)
        # pager
        pager = request.website.pager(
            url="/my/tickets",
            total=ticket_count,
            page=page,
            step=self._items_per_page
        )
        searchbar_sortings = {
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'name': {'label': _('Title'), 'order': 'subject'},
            'stage': {'label': _('Stage'), 'order': 'stage_id'},
        }
        searchbar_filters = {
            'all': {'label': _('All'), 'domain': []},
        }
        # searchbar_inputs = {
        #     'title': {'input': 'title', 'label': _('Search <span class="nolabel"> (in Title)</span>')},
        #     'issue_no': {'input': 'issue_no', 'label': _('Search in Issue Number')},
        #     'date': {'input': 'request_date', 'label': _('Search in date')},
        #     'all': {'input': 'all', 'label': _('Search in All')},
        # }
        searchbar_inputs = {
            'content': {'input': 'content', 'label': _('Search <span class="nolabel"> (in Content)</span>')},
            'message': {'input': 'message', 'label': _('Search in Messages')},
            'customer': {'input': 'customer', 'label': _('Search in Customer')},
            'stage': {'input': 'stage', 'label': _('Search in Stages')},
            'all': {'input': 'all', 'label': _('Search in All')},
        }
        # default sort by value
        if not sortby:
            sortby = 'date'
        order = searchbar_sortings[sortby]['order']
        # default filter by value
        if not filterby:
            filterby = 'all'
        domain += searchbar_filters[filterby]['domain']

        # archive groups - Default Group By 'create_date'
        archive_groups = self._get_archive_groups('project.task', domain)
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        # search
        if search and search_in:
            search_domain = []
            if search_in in ('content', 'all'):
                search_domain = OR([search_domain, ['|', ('name', 'ilike', search), ('description', 'ilike', search)]])
            if search_in in ('customer', 'all'):
                search_domain = OR([search_domain, [('partner_id', 'ilike', search)]])
            if search_in in ('message', 'all'):
                search_domain = OR([search_domain, [('message_ids.body', 'ilike', search)]])
            if search_in in ('stage', 'all'):
                search_domain = OR([search_domain, [('stage_id', 'ilike', search)]])
            domain += search_domain

        # content according to pager and archive selected
        tickets = support_obj.sudo().search(domain, order=order, limit=self._items_per_page, offset=pager['offset'])
        if not partner.child_ids:
            domain = [
                ('create_uid', '=', [request.env.user.id])
            ]
            ticket_count = support_obj.sudo().search_count(domain)
            pager = request.website.pager(
                url="/my/tickets",
                total=ticket_count,
                page=page,
                step=self._items_per_page
            )
            tickets=tickets.sudo().search(domain)
        values.update({
            'tickets': tickets,
            'page_name': 'ticket_page',
            'pager': pager,
            'default_url': '/my/tickets',
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            'searchbar_filters': OrderedDict(sorted(searchbar_filters.items())),
            'filterby': filterby,
        })
        return request.render("website_helpdesk_support_ticket.display_tickets", values)
       
    @http.route(['/my/ticket/<model("helpdesk.support"):ticket>'], type='http', auth="user", website=True)
    def my_ticket(self, ticket=None, access_token=None, **kw):
        attachment_list = request.httprequest.files.getlist('attachment')
        support_obj = http.request.env['helpdesk.support'].sudo().browse(ticket.id)

        # validation of ticket if other user access ticket
        partner = request.env.user.partner_id
        support_object = http.request.env['helpdesk.support']
        domain = [
            ('partner_id', 'child_of', [partner.commercial_partner_id.id])
        ]
        tickets = support_object.sudo().search(domain).ids
        if support_obj.id not in tickets:
            ticket = http.request.env['helpdesk.support'].sudo()

        for image in attachment_list:
            if kw.get('attachment'):
                attachments = {
                           'res_name': image.filename,
                           'res_model': 'helpdesk.support',
                           'res_id': ticket.id,
                           'datas': base64.encodestring(image.read()),
                           'type': 'binary',
                           'datas_fname': image.filename,
                           'name': image.filename,
                       }
                attachment_obj = http.request.env['ir.attachment']
                attachment_obj.sudo().create(attachments)
        if len(attachment_list) > 0:
            group_msg = 'Customer has sent %s attachments to this helpdesk ticket. Name of attachments are: ' % (len(attachment_list))
            for attach in attachment_list:
                group_msg = group_msg + '\n' + attach.filename
            group_msg = group_msg + '\n'  +  '. You can see top attachment menu to download attachments.'
            support_obj.sudo().message_post(body=_(group_msg),
                                            message_type='comment',
                                            subtype="mt_comment",
                                            author_id=support_obj.partner_id.id
                                            )
            customer_msg = _('%s') % (kw.get('ticket_comment'))
            support_obj.sudo().message_post(body=customer_msg,
                                            message_type='comment',
                                            subtype="mt_comment",
                                            author_id=support_obj.partner_id.id)
            return http.request.render('website_helpdesk_support_ticket.successful_ticket_send',{
            })
        if kw.get('ticket_comment'):
            customer_msg = _('%s') % (kw.get('ticket_comment'))
            support_obj.sudo().message_post(body=customer_msg,
                                            message_type='comment',
                                            subtype="mt_comment",
                                            author_id=support_obj.partner_id.id)
            return http.request.render('website_helpdesk_support_ticket.successful_ticket_send',{
            })
        return request.render("website_helpdesk_support_ticket.display_ticket", {'ticket': support_obj, 'token':access_token, 'user': request.env.user})

    @http.route('/page/create/ticket/helpdesk', auth='public', website=True, type='http', csrf_token=True)
    def create_ticket_helpdesk(self, **kw):
        values = {}



        values['ticket_category_ids'] = request.env['ticket.category'].sudo().search([])
        _logger.error('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>ticket_category_ids>>>>>>>', values['ticket_category_ids'])
        values['support_team_ids'] = request.env['support.team'].sudo().search([])

        user_id = request.env['res.users'].sudo().browse(request.uid)
        partner_id=request.env['res.partner'].sudo().search([('name','=',user_id.partner_id.name)])
        contract_list_id=[]
        contract_line_list_id=[]
        for partner in partner_id:
            current_partner_id = request.env['account.analytic.account'].sudo().search(
                [('partner_id', '=', user_id.partner_id.id)])
            if current_partner_id:
                for cur in current_partner_id:
                     contract_list_id.append(cur)
            if partner.parent_id:
                contract_id=request.env['account.analytic.account'].sudo().search([('partner_id','=',partner.parent_id.id)])
                if contract_id:
                    for x in contract_id:
                         contract_list_id.append(x)
        for con in contract_list_id:
            for line in con.product_details_id:
                if line.contract_line_active:
                    contract_line_list_id.append(line)

        #
        # client_ids = request.env['product.details.line'].sudo().search(
        #     [('partner_id', '=', user_id.partner_id.id), ('contract_line_active', '=', True)])
        # # _logger.error('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>client_ids>>>>>>>', values['client_ids'])
        product_list = []
        product_id = 0
        product_detail = 0
        # product_list=list(set(values['client_ids']['product_details_ids']['id']))
        for rec in contract_line_list_id:

            if rec.contract_line_active:
                if rec.product_details_ids:
                    if product_detail != rec.product_details_ids.id and product_id != rec.product_id.id:
                        product_list.append(rec.id)
                    if not rec.product_details_ids.id == product_detail:
                        product_detail = rec.product_details_ids.id
                    if not rec.product_id.id == product_id:
                        product_id = rec.product_id.id

                # product_list.append(rec.product_id)
        values['client_ids']=request.env['product.details.line'].sudo().browse(product_list)

        # print(len( values['client_ids']))
        return request.render('website_helpdesk_support_ticket.website_helpdesk_support_ticket', values)

    @http.route('/populate/data', auth='public', methods=['POST'], website=True, type='http', csrf=False)
    def populate_data(self, **post):
        try:
            # if post['client_id']:
            #    # client_id = request.env['res.partner'].sudo().browse(int(post['client_id']))
            #    # contract_id=request.env['account.analytic.account'].sudo().search([('partner_id', '=', client_id.id)])
            #    contract_id=request.env['product.details.line'].browse(post['client_id'])
            #    product_list=[]
            #    product_vals=[]
            #    for line in contract_id:
            #        for product in line.product_details_id:
            #            if product.active:
            #            # if product.product_id not in product_list:
            #                 product_list.append(product.product_id)
            #    product_list=list(set(product_list))
            #    print(product_list)
            #    for value in product_list:
            #        vals={
            #            'product_id':value.id,
            #             'name':value.name
            #         }
            #        product_vals.append(vals)
            #    return_data = json.dumps(product_vals)
            #    return return_data

            if post['client_id']:
                contract_line_id = request.env['product.details.line'].sudo().browse(int(post['client_id']))
                contract_id = request.env['product.details.line'].sudo().search([('product_details_ids','=',contract_line_id.product_details_ids.id),('contract_line_active', '=', True)])
                product_vals = []
                contract = []
                for rec in contract_id:
                    if rec.contract_line_active:
                        if rec.product_id:
                            product_vals.append(rec.product_id)

                product_vals = list(set(product_vals))
                # contract=list(set(contract_id))

                if product_vals:
                    for con in product_vals:
                        vals = {
                            'product_id': con.id,
                            'name': con.name}
                        contract.append(vals)
                return_data = json.dumps(contract)
                return return_data






        except:
            return json.dumps({'render_data': True})


    @http.route('/populate/environment', auth='public', methods=['POST'], website=True, type='http', csrf=False)
    def populate_environment(self, **post):
        try:
            # if post['product_id']:
            #     environment_id=request.env['product.details.line'].search([('partner_id', '=', int(post['client_id'])),('product_id','=',int(post['product_id'])),('active','=',True)])
            #     env_list = []
            #     env_vals = []
            #     # version_list=[]
            #
            #     for line in environment_id:
            #         if line.active:
            #             if line.environment:
            #                 env_list.append(line.environment)
            #     env_list = list(set(env_list))
            #         # if line.version:
            #         #     version_list.append(line.version)
            #     for value in env_list:
            #         vals = {
            #             'environment': value
            #         }
            #         env_vals.append(vals)
            #
            #     return_data = json.dumps(env_vals)
            #     return return_data
            if post['client_id'] and post['product_id']:
                contract_line_id = request.env['product.details.line'].sudo().browse(int(post['client_id']))
                contract_id = request.env['product.details.line'].sudo().search([('product_details_ids','=',contract_line_id.product_details_ids.id),('contract_line_active', '=', True),('product_id','=',(int(post['product_id'])))])
                env_vals = []
                env_list = []

                for rec in contract_id:
                    if rec.contract_line_active:
                        if rec.environment:
                            env_vals.append(rec.environment)
                env_vals = list(set(env_vals))
                if env_vals:
                    for env in env_vals:
                        vals = {
                            'environment': env,
                        }
                        env_list.append(vals)
            return_data = json.dumps(env_list)
            return return_data


        except:
            return json.dumps({'render_data': True})

    @http.route('/populate/version', auth='public', methods=['POST'], website=True, type='http', csrf=False)
    def populate_version(self, **post):
        try:
            # if post['product_id'] and post['client_id'] and post['environment']:
            #     version_id = request.env['product.details.line'].search(
            #         [('partner_id', '=', int(post['client_id'])), ('product_id', '=', int(post['product_id'])), ('environment', '=', (post['environment'])),('active','=',True)])
            #     ver_list = []
            #     ver_vals = []
            #     # version_list=[]
            #
            #     for line in version_id:
            #         if line.active:
            #             if line.version:
            #                 ver_list.append(line.version)
            #     ver_list = list(set(ver_list))
            #         # if line.version:
            #         #     version_list.append(line.version)
            #     for value in ver_list:
            #         vals = {
            #             'version': value
            #         }
            #         ver_vals.append(vals)
            #
            #     return_data = json.dumps(ver_vals)
            #     return return_data
            if post['client_id'] and post['product_id'] and post['environment']:
                contract_line_id = request.env['product.details.line'].sudo().browse(int(post['client_id']))
                contract_id = request.env['product.details.line'].sudo().search(
                    [('product_details_ids', '=', contract_line_id.product_details_ids.id),
                     ('contract_line_active', '=', True),('environment','=',(str(post['environment']))),('product_id', '=', (int(post['product_id'])))])
                ver_vals = []
                ver_list = []
                for rec in contract_id:
                    if rec.contract_line_active:
                        if rec.version:
                            ver_vals.append(rec.version)
                ver_vals=list(set(ver_vals))
                if ver_vals:
                    for ver in ver_vals:
                        vals = {
                            'version': ver,
                        }
                        ver_list.append(vals)
            return_data = json.dumps(ver_list)
            return return_data
        except:
            return json.dumps({'render_data': True})




# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
