# -*- coding: utf-8 -*

from odoo import models, fields, api,_
from odoo.exceptions import except_orm, ValidationError
import logging
_logger = logging.getLogger(__name__)
class Project(models.Model):
    _inherit = 'project.project'

    sale_order_proj = fields.Many2one('sale.order', 'Sales Order', copy=False)

    sale_line_proj = fields.Many2one('sale.order.line', "Sale Order Item", domain="[('order_id', '=', sale_order_proj)]")
    # domain = "[('order_id', '=', sale_order_proj), ('state', 'in', ['sale', 'done'])]"

    product_id_helpdesk = fields.Many2one(
        'product.product',
        string='Product',
    )
    
    price_rate = fields.Float(
        string='Price / Rate (Company Currency)',
        default=lambda self :('0'),
        copy=False,
    )

    sale_order_id = fields.Many2one('sale.order', 'Sales Order', domain="[('partner_id', '=', partner_id)]",
                                    readonly=False, copy=False)

    @api.onchange('analytic_account_id')
    def onchange_cust(self):
        self.partner_id = self.analytic_account_id.partner_id


    @api.onchange('partner_id')
    def onchange_partner_id(self):
        for project in self:
            project.price_rate = project.partner_id.price_rate
            project.product_id_helpdesk = project.partner_id.product_id_helpdesk

    @api.onchange('sale_line_proj')
    def onchange_sale_line_proj_item(self):
        if self.sale_line_proj:
            task_list = []
            for line in self._origin.task_ids:
                if not self.sale_line_proj.id == line.sale_line_proj.id:
                    task_list.append(line.name)
            if task_list:
                warning_mess = {
                    'title': _('You Cannot Change Sale Order Item'),
                    'message': _(
                        'You have change the Sale Order Item which is not match in Task assign in project you need to change the Sale order Item in this %s task.' % (
                            ','.join(map(str, task_list))))
                }
                if warning_mess:
                    return {'warning': warning_mess}

    # @api.multi
    # def write(self, vals):
    #     res = super(Project, self).write(vals)
    #
    #
    #     if self.sale_line_id:
    #         task_list=[]
    #         for line in self.task_ids:
    #             if not self.sale_line_id.id==line.sale_line_id.id:
    #                 task_list.append(line.name)
    #         if task_list:
    #             # raise ValidationError(_('you have change the Sale Order Item which is not match in Task assign in project you need to change the Sale order Item in this %s task.' % (task_list)))
    #             # print(line)
    #             # _logger.warning("you have change the Sale Order Item which is not match in Task assign in project you need to change the Sale order Item in this %s task.",
    #             #                 task_list)
    #       sale_order_proj = fields.Many2one('sale.order', 'Sales Order',      warning_mess = {
    #                 'title': _('Not enough inventory!'),
    #                 'message': _('you have change the Sale Order Item which is not match in Task assign in project you need to change the Sale order Item in this %s task.' % (task_list))
    #             }
    #     if warning_mess:
    #         return {'warning': warning_mess}
    #
    #     else:
    #         return res



class ProjectTask(models.Model):
    _inherit = 'project.task'

    sale_line_proj = fields.Many2one('sale.order.line', "Sale Order Item")
    sale_order_project = fields.Many2one('sale.order', "Sale Order", related='project_id.sale_order_proj')