{
    'name': 'Kanban Templates',
    'version': '1.0',
    "category": "Kanban",
    'summary': 'Kanban Templates Stages',
    "author": "Siga",
    'website': 'http://www.siganow.com/',
    'depends': ['base', 'project'],
    'data': [
            'security/ir.model.access.csv',
            'view/kanban_view.xml',
            'view/project_inherit_view.xml',
         ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
