from odoo import models, api, fields


class ProjectInherit(models.Model):
    _inherit = 'project.project'

    kanban_template_title = fields.Many2one('kanban.templates',
                                            string="Kanban Template Title",store=True)
    is_created = fields.Boolean("Created")

    @api.multi
    def write(self, vals):
        res = super(ProjectInherit, self).write(vals)
        stage_ids = self.kanban_template_title.stages
        for stage in stage_ids:
            stage.update({'project_ids': [(4,self.id)]})
        return res

    @api.onchange('kanban_template_title')
    def field_read_only(self):
        if self.kanban_template_title:
            self.is_created = True



