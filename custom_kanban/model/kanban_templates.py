from odoo import models, fields, api


class KanbanTemplates(models.Model):
    _name = 'kanban.templates'
    _rec_name = 'template_title'

    template_title = fields.Char("Title", required='True')
    company = fields.Many2many('res.company', 'res_company_rel', 'company_id', 'kanban_id',
                               string="Company")
    stages = fields.Many2many('project.task.type', 'temp_kanban_rel', 'stage_temp_id', 'kanban_temp_id',
                              string="Stages")

