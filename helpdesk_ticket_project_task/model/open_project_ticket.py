from odoo import models, fields, api,_


class PlannedHoursAdd(models.Model):

    _inherit = 'helpdesk.support'

    planned_hours = fields.Float("Planned Hours")
    project_task_view = fields.Integer("View ID")


class ProjectTaskInherited(models.Model):

    _inherit = 'project.task'

    ticket_created = fields.Boolean("Ticket created")

    @api.multi
    def view_project_ticket(self):
        ticket_id = self.env['helpdesk.support'].search([('project_task_view', '=', self.id)])
        action = self.env.ref('website_helpdesk_support_ticket.action_helpdesk_support').read()[0]
        action['domain'] = [('id', 'in', [ticket_id.id])]
        return action