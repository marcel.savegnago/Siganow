from odoo import models,fields,api,_

class CreateHelpdeskProjectWizard(models.TransientModel):

    _name = 'helpdesk.project.task'

    kanban_view_id = fields.Integer("View ID")
    team_id = fields.Many2one('support.team', string="Helpdesk Team")
    category = fields.Selection([('technical', 'Technical'), ('functional', 'Functional'), ('support', 'Support')],
                                string='Category')
    subject_type_id = fields.Many2one('type.of.subject', string="Type of Subject")
    project_id = fields.Many2one('project.project', string="Project")
    user_id = fields.Many2one('res.users', string="Assign to")
    priority = fields.Selection([('0', 'Low'), ('1', 'Middle'), ('2', 'High')],
                                string='Priority', default='0')
    type_ticket_id = fields.Many2one('ticket.type', string="Type of Ticket")
    department_id = fields.Many2one('hr.department', string="Department")
    planned_hours = fields.Float("Planned Hours")
    description = fields.Text("Description")
    ticket_category = fields.Many2one('ticket.category', string="Category")

    @api.model
    def default_get(self, fields_list):

        result_data = super(CreateHelpdeskProjectWizard, self).default_get(fields_list)

        brw = self.env['project.task'].browse(self._context.get('active_ids'))

        result_data.update(
            {'project_id': brw.project_id.id, 'kanban_view_id': brw.id})

        return result_data

    @api.multi
    def create_project_ticket(self):
        list_vals = {
            'project_task_view': self.kanban_view_id,
            'team_id': self.team_id.id,
            # 'category': self.category,
            'ticket_category': self.ticket_category.id,
            'subject_type_id': self.subject_type_id.id,
            'project_id': self.project_id.id,
            'user_id': self.user_id.id,
            'priority': self.priority,
            'type_ticket_id': self.type_ticket_id.id,
            'department_id': self.department_id.id,
            'planned_hours': self.planned_hours,
            'description': self.description,
            'ticket_origin': 'Project',
        }
        create_record = self.env['helpdesk.support'].sudo().create(list_vals)

        brw = self.env['project.task'].browse(self._context.get('active_id'))

        for rec in brw:
            rec.ticket_created = True

    @api.multi
    def create_project_ticket_and_view(self):
        list_vals = {
            'project_task_view': self.kanban_view_id,
            'team_id': self.team_id.id,
            # 'category': self.category,
            'ticket_category': self.ticket_category.id,
            'subject_type_id': self.subject_type_id.id,
            'project_id': self.project_id.id,
            'user_id': self.user_id.id,
            'priority': self.priority,
            'type_ticket_id': self.type_ticket_id.id,
            'department_id': self.department_id.id,
            'planned_hours': self.planned_hours,
            'description': self.description,
            'ticket_origin': 'Project',
        }
        create_record = self.env['helpdesk.support'].sudo().create(list_vals)

        brw = self.env['project.task'].browse(self._context.get('active_id'))

        for rec in brw:
            rec.ticket_created = True

        ticket_id = self.env['helpdesk.support'].search([('project_task_view', '=', self.kanban_view_id)])
        action = self.env.ref('website_helpdesk_support_ticket.action_helpdesk_support').read()[0]
        action['domain'] = [('id', 'in', [ticket_id.id])]
        return action
