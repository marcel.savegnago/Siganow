{
    "name": "Project ticket",
    "version": "1.0",
    "depends": ['project', 'website_job_workorder_request', 'website_helpdesk_support_ticket',
                'helpdesk_ticket_crm_lead','crm_custom'],
    "author": "PlanetOdoo",
    "description": """
         Create helpdesk ticket from project task
""",
    "website": "www.planetodoo.com",
    'images': [],
    "category": "project",
    'summary': 'Create helpdesk ticket from project task',
    "demo": [],
    "data": [
        'wizard/helpdesk_project_task_view.xml',
        'view/inherited_project_task_view.xml',
        'view/inherited_helpdesk_view.xml',
    ],
    'auto_install': False,
    "installable": True,
}