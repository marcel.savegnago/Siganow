from odoo import models, fields, api
from datetime import datetime,date


class HelpdeskProductDetails(models.Model):

    _inherit = 'account.analytic.account'

    product_details_id = fields.One2many('product.details.line', 'product_details_ids', "Product Details")
    product_details_log_id = fields.One2many('product.details.logs', 'product_details_logs_ids', "Logs")


    @api.onchange('partner_id')
    def onchange_customer(self):
        for rec in self:
            if rec.partner_id:
                for line in rec.product_details_id:
                    line.partner_id=rec.partner_id.id

    # def write(self, vals):
    #     list_vals = []
    #     for rec in self.product_details_id:
    #         values = (0, 0, {
    #             'product_id': rec.product_id.id,
    #             'environment': rec.environment,
    #             'version': rec.version,
    #             'start_date': rec.start_date,
    #             'end_date': rec.end_date,
    #             'updated_date': rec.updated_date,
    #             'updated_by': rec.updated_by.id,
    #             'active': rec.active,
    #             'changed_date': datetime.now(),
    #             'changed_by_user': self.env.uid})
    #         list_vals.append(values)
    #     vals.update({'product_details_log_id': list_vals})
    #     res = super(HelpdeskProductDetails, self).write(vals)
    #     return res


class HelpdeskProductDetailsLine(models.Model):

    _name = 'product.details.line'

    product_details_ids = fields.Many2one('account.analytic.account', "Product detail")

    product_id = fields.Many2one('product.product', "Product")
    environment = fields.Char("Environment")
    version = fields.Char("Version")
    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    updated_date = fields.Date("Updated Date")
    updated_by = fields.Many2one('res.users', "Updated By", default=lambda self: self.env.uid)
    contract_line_active = fields.Boolean("Active", compute="valid_warranty", store=True)
    partner_id=fields.Many2one('res.partner','Customer')


    @api.model
    def write(self, vals):
        list_vals = []
        values = {
                'product_details_logs_ids': self.product_details_ids.id,
                'product_id': self.product_id.id,
                'environment': self.environment,
                'version': self.version,
                'start_date': self.start_date,
                'end_date': self.end_date,
                'updated_date': self.updated_date,
                'updated_by': self.updated_by.id,
                'contract_line_active': self.contract_line_active,
                'changed_date': datetime.now(),
                'changed_by_user': self.env.uid,
                'partner_id': self.partner_id.id}
        list_vals.append(values)
        # vals.update({'product_details_log_id': list_vals})
        try_this = self.env['product.details.logs'].create(list_vals)
        res = super(HelpdeskProductDetailsLine, self).write(vals)
        return res

    @api.one
    @api.depends('start_date','end_date')
    def valid_warranty(self):
        today_date = date.today()
        self.contract_line_active = False
        if self.start_date and self.end_date:
            if today_date >= self.start_date and today_date <= self.end_date:
                self.contract_line_active = True


class HelpdeskProductDetailsLogs(models.Model):

    _name = 'product.details.logs'

    product_details_logs_ids = fields.Many2one('account.analytic.account', "Product detail")

    product_id = fields.Many2one('product.product', "Product")
    environment = fields.Char("Environment")
    version = fields.Char("Version")
    start_date = fields.Date("Start Date")
    end_date = fields.Date("End Date")
    updated_date = fields.Date("Updated Date")
    updated_by = fields.Many2one('res.users', "Updated By", default=lambda self: self.env.uid)
    contract_line_active = fields.Boolean("Active", compute="valid_warranty",store=True)
    changed_date = fields.Datetime("Changed Date")
    changed_by_user = fields.Many2one('res.users', string="Changed by user")
    partner_id = fields.Many2one('res.partner', 'Customer')

    @api.one
    @api.depends('start_date', 'end_date')
    def valid_warranty(self):
        today_date = date.today()
        self.contract_line_active = False
        if self.start_date and self.end_date:
            if today_date >= self.start_date and today_date <= self.end_date:
                self.contract_line_active = True