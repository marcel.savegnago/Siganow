# -*- coding: utf-8 -*-

from . import helpdesk_level_config
from . import res_partner
from . import helpdesk_sla
from . import helpdesk_stage_history
from . import helpdesk_ticket
from . import helpdesk_product_details
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
