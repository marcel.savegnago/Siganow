
{
    'name': 'Project & Helpdesk',
    'summary': 'Create New Record after creating record in Project task and Helpdesk Support Ticket',
    'author': 'Planet Odoo',
    'website': 'http://www.planetodoo.com',
    'category': 'Helpdesk',
    'version': '12.0.1.0.0',
    'license': 'AGPL-3',
    'depends': ['base', 'website_helpdesk_support_ticket', 'project'],
    'data': [
            'view/project_helpdesk.xml',
            'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': False,
}
