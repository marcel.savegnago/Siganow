from odoo import api, fields, models,_


class ProjectHelpdesk(models.Model):
    _name = "project.helpdesk"

    record_type = fields.Selection([('ticket', 'Ticket'), ('task', 'Task')], default="ticket")
    helpdesk_id = fields.Many2one('helpdesk.support', 'Helpdesk')
    task_id = fields.Many2one('project.task', 'Task')
    color = fields.Integer("Color")
    ticket_origin = fields.Char("Origin")

    # helpdesk

    h_ticket_origin = fields.Selection([('Helpdesk', 'Helpdesk'), ('CRM', 'CRM'), ('Project', 'Project')], "Origin", default="Helpdesk", related="helpdesk_id.ticket_origin")
    h_environment = fields.Char("Environment", related='helpdesk_id.environment')
    h_version = fields.Char("Version", related='helpdesk_id.version')

    h_stage_id = fields.Many2one('helpdesk.stage.config', string='Stage', related='helpdesk_id.stage_id', store=True)
    h_subject = fields.Char("Subject", related='helpdesk_id.subject', store=True)
    h_type_ticket_id = fields.Many2one('ticket.type', string="Type of Ticket", related='helpdesk_id.type_ticket_id', store=True)
    h_subject_type_id = fields.Many2one('type.of.subject', string="Type of Subject", related='helpdesk_id.subject_type_id', store=True)
    h_user_id = fields.Many2one('res.users', string="Helpdesk Assign To", related='helpdesk_id.user_id', store=True)
    h_email = fields.Char(string="Email", related='helpdesk_id.email', store=True)
    h_company_id = fields.Many2one('res.company', string="Company", related='helpdesk_id.company_id', store=True)
    h_custom_project_task_ids = fields.Many2many('project.task', string='Tasks')
    h_partner_id = fields.Many2one('res.partner', string="Helpdesk Customer", related='helpdesk_id.partner_id', store=True)
    h_phone = fields.Char("Phone", related='helpdesk_id.phone', store=True)
    h_allow_user_ids = fields.Many2many('res.users', string='Allow Users', related="helpdesk_id.allow_user_ids")
    #
    h_team_id = fields.Many2one('support.team', string="Helpdesk Team", related='helpdesk_id.team_id', store=True)
    h_project_id = fields.Many2one('project.project', string="Helpdesk Project", related='helpdesk_id.project_id', store=True)
    h_department_id = fields.Many2one('hr.department', string="Department", related='helpdesk_id.department_id', store=True)
    # h_sale_order_id = fields.Many2one('sale.order', string="Sales Order", related='helpdesk_id.sale_order_id', store=True)
    # h_purchase_id = fields.Many2one('purchase.order', string="purchase Order", related='helpdesk_id.purchase_id', store=True)
    h_team_leader_id = fields.Many2one('res.users', string="Team Leader", related='helpdesk_id.team_leader_id', store=True)
    # h_customer_billing_type = fields.Selection([('prepared_hours', 'Prepaid Hours'),
    #                                             ('postpaid_hours', 'Postpaid Hours')],
    #                                            string='Customer Billing Type', related='helpdesk_id.customer_billing_type', store=True)
    # h_service_support_type_id = fields.Many2one('service.support.type', string="Billing Term", related='helpdesk_id.service_support_type_id', store=True)
    # h_custom_currency_id = fields.Many2one('res.currency', string="Currency", related='helpdesk_id.custom_currency_id', store=True)
    h_priority = fields.Selection([('0', 'Low'), ('1', 'Middle'), ('2', 'High')], string='Priority', related='helpdesk_id.priority', store=True)
    h_category = fields.Selection([('technical', 'Technical'), ('functional', 'Functional'), ('support', 'Support')], string='Category', related='helpdesk_id.category', store=True)
    #
    h_request_date = fields.Datetime('Create Date', related='helpdesk_id.request_date', store=True)
    h_is_close = fields.Boolean("Is Ticket Closed ?", related='helpdesk_id.is_close', store=True)
    # h_level_config_id = fields.Many2one('helpdesk.level.config', string="HelpDesk SLA Level", related='helpdesk_id.level_config_id', store=True)
    # h_dead_line_date = fields.Datetime("DeadLine Date", related='helpdesk_id.dead_line_date', store=True)
    #
    h_close_date = fields.Datetime("Close Date", related='helpdesk_id.close_date', store=True)
    h_total_spend_hours = fields.Float("Total Hours Spent", related='helpdesk_id.total_spend_hours', store=True)
    # h_timesheet_cost_amount = fields.Float("Total Timesheet Cost", related='helpdesk_id.timesheet_cost_amount', store=True)
    #
    h_total_consumed_hours = fields.Float("Total Consumed Hours", related='helpdesk_id.total_consumed_hours', store=True)
    h_total_purchase_hours = fields.Float("Total Purchase Hours", related='helpdesk_id.total_purchase_hours', store=True)
    h_remaining_hours = fields.Float("Remaining Hours", related='helpdesk_id.remaining_hours', store=True)
    #
    h_description = fields.Text("Description", related='helpdesk_id.description', store=True)
    #
    # h_ticket_solution = fields.Html(string="Ticket Solution", related='helpdesk_id.ticket_solution', store=True)
    # h_image1 = fields.Binary("Solution Image 1", related='helpdesk_id.image1', store=True)
    # h_image2 = fields.Binary("Solution Image 2", related='helpdesk_id.image2', store=True)
    # h_image3 = fields.Binary("Solution Image 3", related='helpdesk_id.image3', store=True)
    # h_image4 = fields.Binary("Solution Image 4", related='helpdesk_id.image4', store=True)
    # h_image5 = fields.Binary("Solution Image 5", related='helpdesk_id.image5', store=True)
    #
    h_invoice_line_ids = fields.One2many('support.invoice.line', 'support_id', string='Invoice Lines', related='helpdesk_id.invoice_line_ids', store=True)
    # h_timesheet_line_ids = fields.One2many('account.analytic.line', 'support_request_id', string='Timesheet', related='helpdesk_id.timesheet_line_ids', store=True)
    #
    h_invoice_id = fields.Many2one('account.invoice', string="Invoice Reference", related='helpdesk_id.invoice_id', store=True)
    h_journal_id = fields.Many2one('account.journal', string="Invoice Journal", related='helpdesk_id.journal_id', store=True)
    # h_maintenance_id = fields.Many2one('maintenance.request', string="Maintenance Request", related='helpdesk_id.maintenance_id', store=True)
    #
    h_rating = fields.Selection([('poor', 'Poor'),
                                 ('average', 'Average'),
                                 ('good', 'Good'),
                                 ('very good', 'Very Good'),
                                 ('excellent', 'Excellent')],
                                string='Customer Rating', related='helpdesk_id.rating', store=True)
    h_comment = fields.Text("Customer comment", related='helpdesk_id.comment', store=True)
    #
    # h_ticket_faq_ids = fields.One2many('ticket.faq', 'helpdesk_ticket_id', string="Knowledge Base Q&A", related='helpdesk_id.ticket_faq_ids', store=True)
    #
    # h_is_helpdesk_locked = fields.Boolean("Is Locked", related='helpdesk_id.is_helpdesk_locked', store=True)
    # h_locked_user_id = fields.Many2one('res.users', string="Last Locked By", related='helpdesk_id.locked_user_id', store=True)
    # h_unlock_user_id = fields.Many2one('res.users', string="Last Unlocked By", related='helpdesk_id.unlock_user_id', store=True)
    # h_lock_start_date = fields.Datetime("Lock Start Date", related='helpdesk_id.lock_start_date', store=True)
    # h_lock_end_date = fields.Datetime("Lock End Date", related='helpdesk_id.lock_end_date', store=True)
    #
    # h_stage_history_ids = fields.One2many('helpdesk.stage.history', 'helpdesk_ticket_id', string="Stage History", related='helpdesk_id.stage_history_ids', store=True)

    # project
    # p_number = fields.Char("Number", related='task_id.number', store=True)
    p_project_id = fields.Many2one('project.project', string="Project", related='task_id.project_id', store=True)
    p_user_id = fields.Many2one('res.users', string="Project Assigned to", related='task_id.user_id', store=True)
    # p_sale_line_id = fields.Many2one('sale.order.line', string="Sales Order Item", related='task_id.sale_line_id', store=True)
    p_date_deadline = fields.Date("Deadline", related='task_id.date_deadline', store=True)
    p_tag_ids = fields.Many2many('project.tags', string='Tags')
    #
    p_description = fields.Html("Description", related='task_id.description', store=True)
    #
    p_planned_hours = fields.Float("Planned Hours", related='task_id.planned_hours', store=True)
    # p_progress = fields.Float("Progress", related='task_id.progress', store=True)
    # p_track_planned_hours = fields.One2many('planned.hours.line', 'project_task_ids', string="Logs", related='task_id.track_planned_hours', store=True)
    # p_timesheet_ids = fields.One2many('account.analytic.line', 'task_id', string="Timesheets", related='task_id.timesheet_ids', store=True)
    # p_effective_hours = fields.Float("Hours Spent:", related='task_id.effective_hours', store=True)
    # p_remaining_hours = fields.Float("Remaining Hours:", related='task_id.remaining_hours', store=True)
    #
    # p_job_partner_id = fields.Many2one('res.partner', string="Website Customer", related='task_id.job_partner_id', store=True)
    # p_job_partner_name = fields.Char(string="Website Customer Name", related='task_id.job_partner_name', store=True)
    # p_job_partner_email = fields.Char(string="Website Customer Email", related='task_id.job_partner_email', store=True)
    # p_job_partner_phone = fields.Char(string="Website Customer Phone", related='task_id.job_partner_phone', store=True)
    # p_job_category = fields.Selection(selection = [('new_request', 'New Request'),
    #                                                ('maintenance', 'Maintenance'),
    #                                                ('repair', 'Repair'),
    #                                                ('technical', 'Technical'),
    #                                                ('other', 'Other')], string = "Job Order Category", related='task_id.job_category', store=True)
    # p_is_job_order = fields.Boolean("Is Job Order", related='task_id.is_job_order', store=True)
    #
    # p_purchase_requisition_ids = fields.One2many('material.purchase.requisition', 'joborder_id', string='Purchase Requisitions', related='task_id.purchase_requisition_ids', store=True)
    #
    p_sequence = fields.Integer("Sequence", related='task_id.sequence', store=True)
    p_partner_id = fields.Many2one('res.partner', string="Project Customer", related='task_id.partner_id', store=True)
    # p_price_rate = fields.Float("Price / Rate (Company Currency)", related='task_id.price_rate', store=True)
    # p_product_id_helpdesk = fields.Many2one('product.product', string="Product", related='task_id.product_id_helpdesk', store=True)
    p_email_from = fields.Char(string="Email", related='task_id.email_from', store=True)
    p_email_cc = fields.Char(string="Watchers Emails", related='task_id.email_cc', store=True)
    # p_displayed_image_id = fields.Many2one('ir.attachment', string="Cover Image", related='task_id.displayed_image_id', store=True)
    #
    p_date_assign = fields.Datetime("Assigning Date", related='task_id.date_assign', store=True)
    p_date_last_stage_update = fields.Datetime("Last Stage Update", related='task_id.date_last_stage_update', store=True)
    # p_ticket_id = fields.Many2one('helpdesk.support', string="Helpdesk Ticket", related='task_id.ticket_id', store=True)
    # p_maintenance_request_id = fields.Many2one('maintenance.request', string="Maintenance Request", related='task_id.maintenance_request_id', store=True)
    #Hour(s)
    p_working_hours_open = fields.Float("Hours", related='task_id.working_hours_open', store=True)
    p_working_days_open = fields.Float("Days", related='task_id.working_days_open', store=True)

    @api.onchange('helpdesk_id')
    def project_task_user_fields(self):
        for record in self.helpdesk_id:
            self.h_custom_project_task_ids = record.custom_project_task_ids
            self.h_allow_user_ids = record.allow_user_ids

    @api.onchange('task_id')
    def project_tags_fields(self):
        for record in self.task_id:
            self.p_tag_ids = record.tag_ids


class ProjectAuto(models.Model):

    _inherit = "project.task"

    @api.model
    def create(self, values):
        res = super(ProjectAuto, self).create(values)
        list_vals = {
                    'record_type': 'task',
                    'task_id': res.id,
                    'ticket_origin': 'Project',
                    }
        create_record = self.env['project.helpdesk'].create(list_vals)
        return res


class HelpdeskAuto(models.Model):

    _inherit = "helpdesk.support"

    @api.model
    def create(self, values):
        res = super(HelpdeskAuto, self).create(values)
        list_vals = {
                    'record_type': 'ticket',
                    'helpdesk_id': res.id,
                    'ticket_origin': 'Helpdesk',
                    }
        create_record = self.env['project.helpdesk'].create(list_vals)
        return res