{
    'name': 'Siga ERP Custom',
    'version': '12.0.1.0.0',
    'summary': 'Siga ERP Custom',
    'description': """odoo Siga ERP Custom""",
    'author': 'Planet Odoo',
    'website': 'http://www.planet-odoo.com/',
    'depends': ['base','product','project','website_helpdesk_support_ticket','helpdesk_service_level_agreement','crm_custom'],
    'data': [
            'security/ir.model.access.csv',
            'view/record_group_menu.xml',
            'view/timesheet_wizard_view.xml',
            'view/ticket_start_stop_view.xml',
            'view/project_config_view.xml',
            'view/project_history_view.xml',
            'view/project_pivot_view.xml',
            'view/helpdesk_kanban_inherited.xml',
            'view/project_overview_template.xml',
            # 'view/helpdesk_kanban_view.xml',

                  ],
    'images': [],
    # 'qweb': [
    #     "static/src/xml/helpdesk_kanban_dash.xml",
    # ],

    'installable': True,
    'application': False,
}