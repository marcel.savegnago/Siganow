from odoo import api,fields,models, _


class ProjectConfig(models.TransientModel):

    _inherit = 'res.config.settings'

    group_initial_planned_hrs = fields.Boolean('Control Initial Planned Hours', implied_group='siga_erp_custom.initial_planned_hrs_group')
    group_expense_budget = fields.Boolean('Expenses', implied_group='siga_erp_custom.expense_budget_group')
    group_timesheet_cost = fields.Boolean('Timesheet Costs', implied_group='siga_erp_custom.project_budget_group')

    @api.model
    def get_values(self):
        res = super(ProjectConfig, self).get_values()
        res.update(
                group_initial_planned_hrs=bool(self.env['ir.config_parameter'].sudo().get_param('group_initial_planned_hrs')),
                group_expense_budget=bool(self.env['ir.config_parameter'].sudo().get_param('group_expense_budget')),
                group_timesheet_cost=bool(self.env['ir.config_parameter'].sudo().get_param('group_timesheet_cost')),
            )
        return res

    @api.multi
    def set_values(self):
        super(ProjectConfig, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('group_initial_planned_hrs', bool(self.group_initial_planned_hrs))
        self.env['ir.config_parameter'].sudo().set_param('group_expense_budget', bool(self.group_expense_budget))
        self.env['ir.config_parameter'].sudo().set_param('group_timesheet_cost', bool(self.group_timesheet_cost))


class ProjectSettingView(models.Model):

    _inherit = 'project.project'

    project_budget = fields.Float('Project Budget')
    planned_expense_amount = fields.Float('Planned Expense Amount')
    real_expense_amount = fields.Float('Real Expense Amount')
    project_planned_hours = fields.Float('Planned Hours')

