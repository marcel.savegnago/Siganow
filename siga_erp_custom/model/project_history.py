from odoo import api, fields, models ,_
from datetime import datetime
from odoo.exceptions import UserError, Warning
from dateutil import tz

from odoo.tools.safe_eval import safe_eval


class ProjectTrack(models.Model):

    _inherit = 'project.project'

    track_project = fields.One2many('project.history.line', 'project_ids', "Logs")

    @api.multi
    def write(self, vals):
        list_vals=[]
        values = (0, 0, {'project_budget_old_val': self.project_budget,
                         'expense_amount_old_val': self.planned_expense_amount,
                         'project_planned_hours_old_val': self.project_planned_hours,
                         'project_manager_old_val': self.user_id.name,
                         'customer_old_val': self.partner_id.name,
                         'date_of_change': datetime.now(),
                         'changed_by': self.env.uid})
        list_vals.append(values)
        if vals.get('project_planned_hours') or vals.get('user_id') or vals.get('partner_id')\
                or vals.get('project_budget') or vals.get('planned_expense_amount'):
            vals.update({'track_project': list_vals})
        res = super(ProjectTrack, self).write(vals)
        return res

    @api.onchange('planned_expense_amount')
    def validate_planned_hours_amount(self):
        if self.planned_expense_amount <= self.real_expense_amount:
            raise Warning(_('Real expense amount is greater than planned expense amount'))


class TrackProject(models.Model):

    _name = 'project.history.line'

    project_ids = fields.Many2one('project.project', "Project Id")
    project_budget_old_val = fields.Float("Old Project Budget")
    project_manager_old_val = fields.Char("Old Project Manager")
    customer_old_val = fields.Char("Old Customer")
    project_planned_hours_old_val = fields.Float("Old Planned Hours")
    expense_amount_old_val = fields.Float("Old Expense Amount")
    date_of_change = fields.Datetime("Date of Change")
    changed_by = fields.Many2one('res.users', string="Changed by")


class TrackEmployee(models.Model):

    _name = 'employee.history.line'

    employee_id = fields.Many2one('hr.employee', 'Employee')
    # field_name=fields.Char('Changed Field')
    date_old_value = fields.Char('Date')
    employee_old_value = fields.Char('Employee')
    description_old_value = fields.Char('Description')
    start_old_value = fields.Char('Start Date')
    end_old_value = fields.Char('End Date')
    duration_old_value = fields.Char('Duration')
    # field_new_value = fields.Char('New Value')

    cat_old_value = fields.Char('Product Category')
    user_ids_old_value = fields.Char('Approved By')
    unit_old_value = fields.Char('UOM')
    cost_old_value = fields.Char('Cost')

    date_change = fields.Datetime('Date')
    change_by = fields.Many2one('res.users', string="Changed by")
#
#
# class EmployeeChange(models.Model):
#
#     _inherit = 'hr.employee'
#
#     timesheet_record = fields.One2many('employee.history.line', 'employee_id', "Timesheet Change")
#
#
# class TimesheetChange(models.Model):
#
#     _inherit = 'account.analytic.line'
#
#     def write(self, vals):
#         list_vals = []
#         values = (0, 0, {'date_old_value': self.date,
#                          'employee_old_value': self.employee_id.name,
#                          'description_old_value': self.name,
#                          'start_old_value': self.time_in,
#                          'end_old_value': self.time_out,
#                          'duration_old_value': self.unit_amount,
#                          'date_change': datetime.now(),
#                          'change_by': self.env.uid})
#         list_vals.append(values)
#         emp = self.env['hr.employee'].search([('id', '=', self.employee_id.id)])
#         if emp:
#             if vals.get('date') or vals.get('name') or vals.get('employee_id') or vals.get('time_in')\
#              or vals.get('time_out') or vals.get('unit_amount') or vals.get('date_start'):
#                 emp.write({'timesheet_record': list_vals})
#         res = super(TimesheetChange, self).write(vals)
#         return res

# class TrackEmployee(models.Model):
#
#         _name = 'employee.history.line'
#
#         employee_id = fields.Many2one('hr.employee', 'Employee')
#         # field_name=fields.Char('Changed Field')
#         cat_old_value = fields.Char('Product Category')
#         user_ids_old_value = fields.Char('Approved By')
#         unit_old_value = fields.Char('UOM')
#         start_old_value = fields.Char('Start Date')
#         end_old_value = fields.Char('End Date')
#         cost_old_value = fields.Char('Cost')
#         # field_new_value = fields.Char('New Value')
#         date_change = fields.Datetime('Date')
#         change_by = fields.Many2one('res.users', string="Changed by")

class EmployeeChanges(models.Model):

        _inherit = 'hr.employee'

        timesheet_record = fields.One2many('employee.history.line', 'employee_id', "Timesheet Change")

class TimesheetChanges(models.Model):

        _inherit = 'costs.line'

        def write(self, vals):
            list_vals = []
            values = (0, 0, {'cat_old_value': self.product_category.name,
                             'user_ids_old_value': self.user_ids.name,
                             'unit_old_value': self.unit_ids.name,
                             'start_old_value': self.start_date,
                             'end_old_value': self.end_date,
                             'cost_old_value': self.cost,
                             'date_change': datetime.now(),
                             'change_by': self.env.uid})
            list_vals.append(values)
            emp = self.env['hr.employee'].search([('id', '=', self.cost_ids.id)])
            if emp:
                if 'product_category' or 'user_ids' or 'unit_ids' or 'start_date' or 'end_date' or 'cost' or 'date_start' in vals:
                    emp.write({'timesheet_record': list_vals})
            res = super(TimesheetChanges, self).write(vals)
            return res

