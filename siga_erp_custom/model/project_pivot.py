from odoo import fields, models, tools, api


class PivotReportView(models.Model):

    _name = 'project.pivot.report'
    _auto = False

    name=fields.Many2one('project.project', string="Project")
    project_subtask=fields.Many2one('project.task', string="Task")
    customer=fields.Many2one('res.partner',string='Customer')
    planned_hrs=fields.Float('Planned Hours',group_operator="avg")
    project_bud=fields.Float('Budget',group_operator="avg")
    planned_exp_amt=fields.Float('Planned Expense Amount',group_operator="avg")
    real_exp_amt=fields.Float('Real Expense Amount',group_operator="avg")
    product=fields.Many2one('product.product',string='Product')
    sale_order_id=fields.Many2one('sale.order', string='Sale Order')
    manager=fields.Many2one('res.partner',string='Project Manager')

    def _select(self):
        select_str = """min(pp.id) as id,pp.id as name,pp.project_planned_hours as planned_hrs,pp.project_budget as project_bud,
        pp.planned_expense_amount as planned_exp_amt,pp.real_expense_amount as real_exp_amt,
        pp.partner_id as customer,pp.product_id_helpdesk as product,so.id as sale_order_id,pn.id as manager,pt.id as project_subtask
        """
        return select_str

    def _from(self):
        from_str = """
            project_project as pp
            left join (select * from project_task)
            as pt on pp.id=pt.project_id
            left join (select * from res_partner)
            as rs on pp.id=rs.id
            left join (select * from product_product)
            as pd on pp.id=pd.id
            left join (select * from sale_order)
            as so on pp.id=so.id
            left join (select * from res_users)
            as ru on pp.id =ru.id 
            left join (select * from res_partner)
            as pn on ru.partner_id = pn.id
        """
        return from_str

    def _group_by(self):
        group_by_str = """
            group by pp.id ,pp.partner_id,pp.product_id_helpdesk,so.id,pn.id,pt.id
            """
        return group_by_str

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as ( SELECT
        %s
        FROM %s
        %s
        )""" % (self._table, self._select(), self._from(),self._group_by()))
        print(True)


class HelpdeskKanban(models.Model):

    _name = 'helpdesk.support.template'

    @api.model
    def get_status_details(self):
        tech_count = self.env['helpdesk.support'].sudo().search_count([('category', '=', 'technical')])
        fun_count = self.env['helpdesk.support'].sudo().search_count([('category', '=', 'functional')])
        support_count = self.env['helpdesk.support'].sudo().search_count([('category', '=', 'support')])

        data = []

        values = {
            'technical': tech_count,
            'functional': fun_count,
            'support': support_count,
            }
        data.append(values)
        return data

