from odoo import api,fields,models, _
from datetime import datetime
import re
from dateutil import relativedelta
from odoo.exceptions import ValidationError,UserError


class HelpdeskStartWizard(models.TransientModel):

    _name = 'helpdesk.task.wizard'

    date=fields.Date("Date")
    unit_amount=fields.Float("Duration")
    name=fields.Text("Task Description")
    date_start = fields.Datetime("Start")
    date_stop = fields.Datetime("Stop")
    helpdesk_id=fields.Many2one('helpdesk.support', 'help id')
    # product_category = fields.Selection([('outsourcing', 'Outsourcing'), ('helpdesk', 'Helpdesk'),
    #                                      ('project', 'Project'), ('lead_opp', 'Lead/Opportunity')],
    #                                     string="Product Category")
    product_category = fields.Many2one('product.category', string='Product Category')
    state = fields.Selection(string="States", selection=[('a1', 'Activity Stopped'),
                                                         ('a2', 'Activity Started'),
                                                          ],
                             default="a1", track_visibility='onchange', )



    @api.multi
    def set_activity(self):
        if self.helpdesk_id:
            values = {'name': self.name,'product_category':self.product_category.id,'unit_amount': self.unit_amount,
                      'date_start':self.date_start,'date_stop':self.date_stop,'account_id':1,self.state:'a1'}
            self.helpdesk_id.write({'timesheet_line_ids': [(0, 0, values)]})
            # self.env['account.analytic.line'].create(values)
            self.helpdesk_id.write({'state': 'a1'})

    @api.multi
    def cancel(self):
        return {'type': 'ir.actions.act_window_close'}


class HelpdeskStart(models.Model):

    _inherit = 'helpdesk.support'

    date_start=fields.Datetime("Start")
    date_stop=fields.Datetime("Stop")
    activity_description=fields.Char("Activity Description")
    active_timesheet=fields.Boolean('Timesheet Active')
    # product_category = fields.Selection([('outsourcing', 'Outsourcing'), ('helpdesk', 'Helpdesk'),
    #                                      ('project', 'Project'), ('lead_opp', 'Lead/Opportunity')],
    #                                     string="Product Category")
    product_category = fields.Many2one('product.category', string='Product Category')

    state = fields.Selection(string="States", selection=[('a1', 'Activity Stopped'),
                                                         ('a2', 'Activity Started'),
                                                          ],
                             default="a1", track_visibility='onchange', )
    read_timesheet = fields.Boolean('Timesheet Readable')
    # cost_consumed_hours = fields.Float("Cost of Consumed Hours")

    @api.model
    def default_get(self, fields_list):
        result = super(HelpdeskStart, self).default_get(fields_list)

        if self.env.user.has_group('siga_erp_custom.helpdesk_timesheet_start_stop'):
            result.update({'active_timesheet': True})
        if self.env.user.has_group('crm_custom.timesheet_for_support_task'):
            result.update({'read_timesheet': True})
        return result


    @api.multi
    def start_ticket(self):
        ticket = self.env['helpdesk.support'].search([])
        for item in ticket:
            if item.state == 'a2':
                raise UserError(_('Finished the ongoing activity first in %s,' % (item.name)))
        for rec in self:
            if rec.stage_id.name == 'Closed':
                raise UserError(_('You cannot not start the closed activity'))
        self.date_start = datetime.now()
        self.write({'state': 'a2'})
        print(str(self.date_start))

    @api.multi
    def stop_ticket(self):
        self.date_stop = datetime.now()
        # self.write({'state': 'a1'})
        date1 = datetime.strptime(str(self.date_start), '%Y-%m-%d %H:%M:%S.%f')
        date2 = datetime.strptime(str(self.date_stop), '%Y-%m-%d %H:%M:%S.%f')
        r = relativedelta.relativedelta(date2, date1)
        time_min =r.days*1440 + r.hours * 60 + r.minutes
        time = time_min / 60

        category = ""
        if self.analytic_account_id.subscription_product_line_ids:
            for rec in self.analytic_account_id.subscription_product_line_ids:
                category = rec.product_id.categ_id.id
                break
        elif self.sale_order_id:
            for item in self.sale_order_id.order_line:
                category = item.product_id.categ_id.id
                break

        if self.date_stop:
            view = self.env.ref('siga_erp_custom.helpdesk_timesheet_wizard_form_view')
            ctx = dict(self.env.context or {})
            ctx.update({
                'default_unit_amount': time,
                'default_date_start': date1,
                'default_date_stop': date2,
                'default_helpdesk_id': self.id,
                'default_product_category': category,
            })
            return {
                'name': _('Stop Activity?'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'helpdesk.task.wizard',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': ctx,
            }


class TimesheetHelpStart(models.Model):

    _inherit = 'account.analytic.line'

    activity_description = fields.Char("Activity Description")


    @api.onchange('date','user_id','employee_id')
    def change_product_cats(self):
        for each in self:
            category = ""
            if each.support_request_id.analytic_account_id.subscription_product_line_ids:
                for rec in each.support_request_id.analytic_account_id.subscription_product_line_ids:
                    category = rec.product_id.categ_id.id
            elif each.support_request_id.sale_order_id:
                for item in each.support_request_id.sale_order_id.order_line:
                    category = item.product_id.categ_id.id
            # for rec in each.timesheet_line_ids:
            each.product_category = category
            each.account_id=1


class ResConfigHelpdesk(models.TransientModel):

    _inherit = 'res.config.settings'

    group_helpdesk_timesheet_start_stop = fields.Boolean('Start and Stop', implied_group='siga_erp_custom.helpdesk_timesheet_start_stop')
    timesheet_id=fields.Many2one('project.task',string='Timesheet id')

    @api.onchange('group_helpdesk_timesheet_start_stop')
    def check_status(self):
        if self.group_helpdesk_timesheet_start_stop==True:
            for item in self.env['helpdesk.support'].search([]):
                item.write({'active_timesheet':True})
        else:
            for item in self.env['helpdesk.support'].search([]):
                item.write({'active_timesheet': False})

    @api.model
    def get_values(self):
        res = super(ResConfigHelpdesk, self).get_values()
        res.update(
            group_helpdesk_timesheet_start_stop=bool(self.env['ir.config_parameter'].sudo().get_param('group_helpdesk_timesheet_start_stop')),
        )
        return res

    @api.multi
    def set_values(self):
        super(ResConfigHelpdesk, self).set_values()
        self.env['ir.config_parameter'].sudo().set_param('group_helpdesk_timesheet_start_stop', bool(self.group_helpdesk_timesheet_start_stop))
