{
    'name': 'Employee_reports',
    'version': '12.0.1.0.0',
    'summary': 'Employee Reports',
    'description': """odoo employee_reports""",
    'author': 'Planet Odoo',
    'website': 'http://www.planet-odoo.com/',
    'depends': ['base','website_helpdesk_support_ticket'],
    'data': [
            'view/report_pivot_view.xml',
        'security/ir.model.access.csv',

                  ],
    'images': [],

    'installable': True,
    'application': False,
}