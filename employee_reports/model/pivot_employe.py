from odoo import fields, models, tools, api


class PivotReportView(models.Model):

    # _inherit = 'hr.employee'
    _name = 'employee.report.view'
    _auto = False

    name = fields.Many2one('hr.employee', string='Employee')
    timesheet_hrs = fields.Float("Time sheet Hours")
    job_pos = fields.Many2one("hr.job", "Job Position")
    manager = fields.Many2one("hr.employee", "Manager")
    coach = fields.Many2one("hr.employee", "Coach")
    department = fields.Many2one("hr.department", "Department")
    ticket_name = fields.Many2one('helpdesk.support', string='Tickets')
    company = fields.Many2one('res.partner', string='Company')
    salary_info = fields.Float('Employee Salary')
    lead_opportunity = fields.Many2one('crm.lead', string="Lead/Opportunity")
    total_cost=fields.Float("Total Cost")
    project_work_hours = fields.Float('Project Worked Hours')
    ticket_work_hours = fields.Float('Ticket Worked Hours')
    billable_hrs = fields.Float('Billable Hours')
    non_billable_hrs = fields.Float('Non Billable Hours')
    cap_usage = fields.Float('Capacity Usage')
    project_cap_usage = fields.Float('Capacity Usage in Project')
    ticket_cap_usage = fields.Float('Capacity Usage in Ticket')

    # contact info
    emp_location=fields.Char('Work Location')
    emp_email=fields.Char('Work Email')
    emp_mobile=fields.Char('Work Mobile')
    emp_timezone=fields.Char('Timezone')
    contract=fields.Many2one('hr.contract.type',string='Contract Type')
    job_title=fields.Char('Job Title')

    # citizenship and other info
    identification_id=fields.Char('Identification Id')
    nationality=fields.Many2one('res.country',string='Nationality')
    passport=fields.Char('Passport')
    ban=fields.Many2one('res.partner.bank',string='Bank Account Number')

    # private_address
    private_address=fields.Many2one('res.partner',string='Private Address')
    emergency_contact=fields.Char('Emergency Contact')
    emergency_phone=fields.Char('Emergency Phone')
    km_home=fields.Integer('Km Home-Work')

    # status
    gender=fields.Char('Gender')
    marital_status=fields.Char('Marital Status')
    noc=fields.Integer('No. of children')

    # Birth
    dob=fields.Date('Date of Birth')
    pob=fields.Char('Place of Birth')
    cob=fields.Many2one('res.country',string='Country of Birth')

    # work permit
    visa_no=fields.Char('Visa No')
    permit_no=fields.Char('Permit No')
    visa_exp=fields.Char('Visa Expire Date')

    # Education
    certificate_level=fields.Char('Certification Level')
    fos=fields.Char('Field of Study')
    study_school=fields.Char('Study School')

    # expenses
    expense=fields.Many2one('res.users',string='Expense Responsible')

    # status
    related_user=fields.Many2one('res.users',string='Related User')

    # Time sheet Appointments Rule
    behind_days=fields.Integer('No of Days Behind Allowed')
    ahead_days=fields.Integer('No of Days Ahead Allowed')

    # current contact
    medic=fields.Date("Medical Exam")
    company_vehicle=fields.Char("Company Vehicle")

    # attendance
    badge=fields.Char("Badge Id")
    workload=fields.Integer('Workload')
    # manual_attendances=fields.Boolean('Manual Attendance')

    # other info
    prod_factor=fields.Float('Productive Factor')
    emp_level=fields.Many2one('employee.level',string='Employee Level')

    # project
    project = fields.Many2one("project.project", "Project")
    task = fields.Many2one("project.task", "Task")
    project_manager=fields.Many2one('res.users', string='Project Manager')
    project_customer=fields.Many2one('res.partner', string='Project Customer')
    project_sale = fields.Many2one('sale.order', string='Project Sale Order')

    #timesheet
    product_cat=fields.Many2one('product.category', string='Product Category')
    timesheet_des=fields.Char('Timesheet Description')
    start_date=fields.Datetime('Timesheet Start Date')
    end_date=fields.Datetime('Timesheet End Date')
    billable=fields.Boolean('Billable')

    # helpdesk
    helpdesk_customer=fields.Many2one('res.partner',string='Helpdesk Customer')
    helpdesk_sale=fields.Many2one('sale.order', string='Helpdesk Sale Order')
    helpdesk_product=fields.Many2one('product.product',string='Helpdesk Product')
    helpdesk_env=fields.Char('Helpdesk Environment')
    helpdesk_version=fields.Char('Helpdesk Version')

    def _select(self):
        select_str = """min(h.id) as id,h.id as name,h.address_id as company,h.job_id as job_pos, h.parent_id as manager,
                        h.coach_id as coach,
                        h.department_id as department,ts.unit_amount as timesheet_hrs,ts.project_id as project,
                        ts.task_id as task,ts.support_request_id as ticket_name,con.wage as salary_info,
                        he.lead_opportunity_id as lead_opportunity,
                        sum(ts.tot_cost) as total_cost,ts.product_category as product_cat,ts.name as timesheet_des,
                        ts.date_start as start_date,ts.date_stop as end_date,ts.billable as billable,
                        h.project_worked_hours as project_work_hours,h.ticket_worked_hours as ticket_work_hours,
                        h.billable_hours as billable_hrs,h.non_billable_hours as non_billable_hrs,
                        h.capacity_usage as cap_usage,h.project_capacity_usage as project_cap_usage,
                        h.ticket_capacity_usage as ticket_cap_usage,
                        h.work_location as emp_location,
                        h.work_email as emp_email,
                        h.mobile_phone as emp_mobile,
                        h.contract_type as contract,
                        h.job_title as job_title,
                        h.passport_id as passport,
                        h.identification_id as identification_id,
                        h.country_of_birth as nationality,
                        h.bank_account_id as ban,
                        h.address_home_id as private_address,
                        h.emergency_contact as emergency_contact,
                        h.emergency_phone as emergency_phone,
                        h.km_home_work as km_home,
                        h.gender as gender,
                        h.marital as marital_status,
                        h.children as noc,
                        h.birthday as dob,
                        h.place_of_birth as pob,
                        h.country_of_birth as cob,
                        h.visa_no as visa_no,
                        h.permit_no as permit_no,
                        h.visa_expire as visa_exp,
                        h.certificate as certificate_level,
                        h.study_field as fos,
                        h.study_school as study_school,
                        h.expense_manager_id as expense,
                        h.user_id as related_user,
                        h.days_behind as behind_days,
                        h.days_ahead as ahead_days,
                        h.medic_exam as medic,
                        h.vehicle as company_vehicle,
                        h.barcode as badge,
                        h.workload as workload,
                        h.productive_factor as prod_factor,
                        h.employee_level as emp_level,
                        help.partner_id as helpdesk_customer,
                        help.sale_order_id as helpdesk_sale,
                        help.product_id as helpdesk_product,
                        help.environment as helpdesk_env,
                        help.version as helpdesk_version,
                        proj.user_id as project_manager,
                        proj.partner_id as project_customer,
                        proj.sale_order_proj as project_sale
                    """
        return select_str

    def _from(self):
        from_str = """
                   hr_employee as h
                   left join (select * from account_analytic_line)
                   as ts on h.id=ts.employee_id
                   left join (select * from hr_contract)
                   as con on h.id=con.employee_id
                   left join (select * from hr_expense)
                   as he on h.id=he.employee_id 
                   left join (select * from hr_contract_type)
                   as ct on h.id=ct.id
                   left join (select * from res_country)
                   as rc on h.id=rc.id
                   left join (select * from res_partner_bank)
                   as rpc on h.id=rpc.id
                   left join (select * from res_partner)
                   as rp on h.id=rp.id
                   left join (select * from res_users)
                   as ru on h.id=ru.id
                   left join (select * from employee_level)
                   as el on h.id=el.id
                   left join (select * from helpdesk_support)
                   as help on h.id=help.id
                   left join (select * from project_project)
                   as proj on h.id=proj.id
             """
        return from_str

    def _group_by(self):
        group_by_str = """
              group by h.id ,h.job_id,ts.unit_amount,h.job_id,h.coach_id, h.department_id,ts.project_id,ts.task_id,
              ts.support_request_id,ts.unit_amount,con.wage,h.address_id,he.lead_opportunity_id,ts.product_category,
              ts.name,ts.date_start,ts.date_stop,ts.billable,help.partner_id,help.sale_order_id,help.product_id,
              help.environment,help.version,proj.user_id,proj.partner_id,proj.sale_order_proj
           """
        return group_by_str

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as ( SELECT
                  %s
                  FROM %s
                  %s
                  )""" % (self._table, self._select(),self._from(),self._group_by()))
        print(True)


class EmployeeChange(models.Model):

    _inherit = 'hr.employee'

    category_change_ids = fields.Many2many(
        'hr.employee.category', 'employee_category_rel',
        'emp_id', 'category_id',
        string='Tags')

    project_worked_hours=fields.Float(string='Project Worked Hours',compute='_compute_project_worked_hours',store=True)
    ticket_worked_hours=fields.Float(string='Ticket Worked Hours',compute='_compute_ticket_worked_hours',store=True)
    billable_hours=fields.Float(string='Billable Hours',compute='_compute_bill_hours',store=True)
    non_billable_hours=fields.Float(string='Non Billable Hours',compute='_compute_non_bill_hours',store=True)
    capacity_usage=fields.Float(string='Capacity Usage',compute='_compute_capacity_usage',store=True)
    project_capacity_usage=fields.Float(string='Capacity Usage in Project',compute='_compute_project_capacity_usage',store=True)
    ticket_capacity_usage=fields.Float(string='Capacity Usage in Ticket',compute='_compute_ticket_capacity_usage',store=True)

    @api.one
    @api.depends('emp_timesheet_line.unit_amount')
    def _compute_project_worked_hours(self):
        timesheet=self.env['account.analytic.line'].search([('employee_id','=',self.id)])
        for rec in timesheet:
            if rec.project_id:
                self.project_worked_hours += rec.unit_amount

    @api.one
    @api.depends('emp_timesheet_line.unit_amount')
    def _compute_ticket_worked_hours(self):
        timesheet = self.env['account.analytic.line'].search([('employee_id', '=', self.id)])
        for rec in timesheet:
            if rec.support_request_id:
                self.ticket_worked_hours += rec.unit_amount

    @api.one
    @api.depends('emp_timesheet_line.billable')
    def _compute_bill_hours(self):
        timesheet = self.env['account.analytic.line'].search([('employee_id', '=', self.id)])
        for rec in timesheet:
            if rec.billable != False:
                self.billable_hours += rec.unit_amount

    @api.one
    @api.depends('emp_timesheet_line.billable')
    def _compute_non_bill_hours(self):
        timesheet = self.env['account.analytic.line'].search([('employee_id', '=', self.id)])
        for rec in timesheet:
            if rec.billable == False:
                self.non_billable_hours += rec.unit_amount

    @api.one
    @api.depends('emp_timesheet_line.unit_amount')
    def _compute_capacity_usage(self):
        timesheet = self.env['account.analytic.line'].search([('employee_id', '=', self.id)])
        worked_hours=0.0
        for rec in timesheet:
                worked_hours += rec.unit_amount
        if self.workload:
            self.capacity_usage=worked_hours/self.workload

    @api.one
    @api.depends('emp_timesheet_line.unit_amount')
    def _compute_project_capacity_usage(self):
        timesheet = self.env['account.analytic.line'].search([('employee_id', '=', self.id)])
        project_worked_hours = 0.0
        for rec in timesheet:
            if rec.project_id:
                project_worked_hours += rec.unit_amount
        if self.workload:
            self.project_capacity_usage = project_worked_hours / self.workload

    @api.one
    @api.depends('emp_timesheet_line.unit_amount')
    def _compute_ticket_capacity_usage(self):
        timesheet = self.env['account.analytic.line'].search([('employee_id', '=', self.id)])
        ticket_worked_hours = 0.0
        for rec in timesheet:
            if rec.support_request_id:
                ticket_worked_hours += rec.unit_amount
        if self.workload:
            self.ticket_capacity_usage = ticket_worked_hours / self.workload


