# -*- coding: utf-8 -*-

{
    "name": "Merge Project Tasks",
    "version": "1.0",
    "category": "Project",
    "author": "Planet-Odoo",
    "website": "http://www.planet-odoo.com",
    "depends": ["base","project"],
    "data": [
        'security/ir.model.access.csv',
        'views/res_employee_view.xml',
        'views/project_data_view.xml',
        'wizards/project_merge_task_wizard_view.xml',
    ],
    "qweb": [],
    "installable": True,
    "auto_install": False,
}
