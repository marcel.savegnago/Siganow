# -*- coding: utf-8 -*-

from odoo import api, fields, models


class EmployeeHR(models.Model):
    _inherit = 'hr.employee'

    productive_factor = fields.Float("Productive Factor")
    # employee_level = fields.Selection([('trainee', "Trainee Jr"),
    #                                    ('pleno', "Pleno"),
    #                                    ('senior', "Senior")], string="Employee Level")
    employee_level = fields.Many2one('employee.level',string='Employee Level')
    contract_type = fields.Many2one('hr.contract.type',string='Contract Type')
#     timesheets_id = fields.One2many('emp.hr.line', 'timesheet_ids', "timesheet")
#
# class EmployeeHRLine(models.Model):
#
#     _name = 'emp.hr.line'
#
#     timesheet_ids = fields.Many2one('hr.employee', "ids")
#     product_category = fields.Char("Product Category")
#     start_date = fields.Date("Start Date")
#     end_date = fields.Date("End Date")
#     cost = fields.Float("Cost")
#     uom = fields.Char("UOM")
#     Approved_by = fields.Many2one('hr.employee', "Approved by")
#     default = fields.Char("Default")


class EmployeeLevel(models.Model):

    _name = 'employee.level'

    name=fields.Char("Employee Level")

