from odoo import http
from odoo.http import request


class KsWebsiteDashboard(http.Controller):

    @http.route(['/dashboard'], type='json', auth='public', website=True)
    def ks_dashboard_handler(self, **post):
        dashboard_records = request.env['ks_dashboard_ninja.board'].sudo().search_read([])

        return dashboard_records

    @http.route(['/dashboard/data'], type='json', auth='public', website=True)
    def ks_dashboard_data_handler(self, **post):
        dashboard_config = {}
        ks_company_id = ['|', ['ks_company_id', '=', False]]

        ks_dashboard_id = post.get('kwargs').get('id')
        ks_type = post.get('kwargs').get('type')

        if self.ks_check_login_user_or_not():
            ks_company_id.append(['ks_company_id', '=', request.env.user.company_id.id])
        else:
            ks_company_id.append(['ks_company_id', '=', request.website.company_id.id])

        if ks_dashboard_id != 0:
            if ks_type == 'user_data':
                if request.env.user in request.env['res.users'].sudo().search([]):
                    dashboard_config = request.env['ks_dashboard_ninja.board'].ks_fetch_dashboard_data(ks_dashboard_id,
                                                                                                       ks_company_id)
            else:
                dashboard_config = request.env['ks_dashboard_ninja.board'].sudo().with_context(
                    force_company=ks_company_id).ks_fetch_dashboard_data(ks_dashboard_id, ks_company_id)
            dashboard_config['ks_dashboard_manager'] = False
            return dashboard_config

        return {}

    @http.route(['/fetch/item/update'], type='json', auth='public', website=True)
    def ks_fetch_item_controller(self, **post):
        item_records = {}
        ks_item_id = post.get('kwargs').get('item_id')
        ks_dashboard_id = post.get('kwargs').get('dashboard')
        ks_type = post.get('kwargs').get('type')

        if ks_type == 'user_data':
            if request.env.user in request.env['res.users'].sudo().search([]):
                item_records = request.env['ks_dashboard_ninja.board'].ks_fetch_item(ks_item_id, ks_dashboard_id)
        else:
            item_records = request.env['ks_dashboard_ninja.board'].sudo().ks_fetch_item(ks_item_id, ks_dashboard_id)

        return item_records

    @http.route(['/fetch/drill_down/data'], type='json', auth='public', website=True)
    def ks_fetch_drill_down_data_controller(self, **post):
        item_records = {}
        if self.ks_check_login_user_or_not():
            ks_company_id = request.env.user.company_id.id
        else:
            ks_company_id = request.website.company_id.id
        if post.get('kwargs').get('type') == 'user_data':
            if request.env.user in request.env['res.users'].sudo().search([]):
                item_records = request.env['ks_dashboard_ninja.item'].ks_fetch_drill_down_data(
                    post.get('kwargs').get('item_id'), post.get('kwargs').get('domain'), post.get('kwargs')
                        .get('sequence'))
        else:
            item_records = request.env['ks_dashboard_ninja.item'].sudo().with_context(force_company=ks_company_id
                                                                                      ).ks_fetch_drill_down_data(
                post.get('kwargs').get('item_id'), post.get('kwargs').get('domain'), post.get('kwargs').get('sequence'))

        return item_records

    @http.route(['/check/user'], type='json', auth='public', website=True)
    def ks_check_user_login(self, **post):
        return self.ks_check_login_user_or_not()

    def ks_check_login_user_or_not(self):
        if request.env.user in request.env['res.users'].sudo().search([]):
            return True
        return False
