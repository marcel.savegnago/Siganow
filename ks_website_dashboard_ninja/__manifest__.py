# -*- coding: utf-8 -*-
{
    'name': "Website Dashboard ninja",

    'summary': """
        Revamp your Odoo Dashboard like never before! It is one of the best dashboard odoo apps in the market. 
        """,

    'description': """
        V12 Dashboard
        Dashboard v12.0,
        Odoo Dashboard v12.0,
        Website Dashboard v12.0,
        Odoo Website Dashboards,
        Best Website Dashboard Apps
        Best Dashboard Apps
        Best Odoo Apps
        Dashboard For Websites
        Odoo Dashboard apps,
        Best Odoo Dashboard Apps
        Dashboard apps,
        Dashboards for Websites
        HR Dashboard Apps,
        Sales Dashboard Apps, 
        inventory Dashboard Apps, 
        Lead Dashboards, 
        Opportunity Dashboards,
        CRM Dashboards,
        Best POS Apps
        POS Dashboards,
        Connectors
        Web Dynamic Apps,
        Report Import/Export,
        Date Filter Apps	
        Tile Dashboard Apps
        Dashboard Widgets,
        Dashboard Manager Apps,
        Debranding Apps
        Customize Dashboard Apps
        Graph Dashboard Apps,
        Charts Dashboard Apps
        Invoice Dashboard Apps
        Project management Apps
    """,

    'author': "Ksolves India Pvt. Ltd.",
    'license': 'OPL-1',
    'currency': 'EUR',
    'price': 49.0,
    'website': "https://www.ksolves.com",
    'maintainer': 'Ksolves India Pvt. Ltd.',
    'live_test_url': 'https://dashboardninja.kappso.com',
    'category': 'Tools',
    'version': '1.0.0',
    'support': 'sales@ksolves.com',
    'images': ['static/description/banners/banner.png'],
    'depends': ['base', 'website', 'ks_dashboard_ninja'],

    'data': [
        'views/ks_assets.xml',
        'views/ks_snippets.xml',
        'views/ks_dashboard_ninja.xml',
    ],

}
