{
    "name": "Access Right",
    "version": "1.0",
    "depends": ['project', 'hr_timesheet'],
    "author": "PlanetOdoo",
    "description": """
         Access Rights for project initially planned hours
""",
    "website": "www.planetodoo.com",
    'images': [],
    "category": "project",
    'summary': 'Access to initially planned hours',
    "demo": [],
    "data": [
        'security/access_groups.xml',
        'security/ir.model.access.csv',
        'views/inhertited_project_view.xml',
    ],
    'auto_install': False,
    "installable": True,
}