from datetime import datetime
from odoo import api,fields,models, _
from odoo.exceptions import UserError, Warning

class CustomProject(models.Model):

    _inherit = 'project.task'

    block_planned_hours_access = fields.Boolean("Block access planned hours", compute="access_planned_hours")
    track_planned_hours = fields.One2many('planned.hours.line', 'project_task_ids', "Logs")

    @api.model
    def write(self, vals):
        for rec in self:
            store_panned_hours = vals.get('planned_hours')
            list_vals = {
                        'planned_hours_old_val': rec.planned_hours,
                        'new_value': store_panned_hours,
                        'date_of_change': datetime.now(),
                        'changed_by_user': rec.env.uid,
                        'project_task_ids': rec.id}
            if store_panned_hours:
                values_list = self.env['planned.hours.line'].create(list_vals)
        res = super(CustomProject, self).write(vals)
        return res


        # values_list = self.env['planned.hours.line'].create(list_vals)
    @api.depends('name')
    def access_planned_hours(self):
        block_planned_hours_access = self.user_has_groups('access_right.group_readonly_planned_hours')
        admin = self.env.user.name
        self.block_planned_hours_access = False
        if block_planned_hours_access and admin != 'Administrator':
            self.block_planned_hours_access = True
        return self.block_planned_hours_access

    @api.onchange('planned_hours')
    def cal_planned_hours_control(self):
        if self.user_has_groups('siga_erp_custom.initial_planned_hrs_group'):
            task_search = self.env['project.task'].search([('project_id', '=', self.project_id.name), ('id', '!=', self._origin.id)])
            total_planned_hours_vals = 0
            for rec in task_search:
                total_planned_hours_vals += rec.planned_hours
            total_planned_hours_vals += self.planned_hours
            if total_planned_hours_vals <= self.project_id.project_planned_hours:
                pass
            else:
                raise Warning(_('Your planned Hours is exceeding the Project Planned hours'))


class TrackPlannedHourChanged(models.Model):

    _name = 'planned.hours.line'

    project_task_ids = fields.Many2one('project.task', "Project_task_ids")
    planned_hours_old_val = fields.Float("Old Planned Hours Value")
    new_value = fields.Float("New Planned Hours Value")
    date_of_change = fields.Datetime("Date of Change")
    changed_by_user = fields.Many2one('res.users', string="Changed by user")